﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Mpoko.DAO;
using Mpoko.CMS;

namespace CMSApi
{
    public partial class Responder : System.Web.UI.Page
    {
        MPOKO_HISAEntities BackC = new MPOKO_HISAEntities();
        CEDA_CMSEntities BackC2 = new CEDA_CMSEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Page.Response.ContentType = "text/xml";
                System.IO.StreamReader reader = new System.IO.StreamReader(Page.Request.InputStream);
                string xmlData = reader.ReadToEnd();
                if (xmlData!=""|| xmlData !=null)
                {
                savelog(xmlData);
                }
               
                System.IO.StreamWriter SW = default(System.IO.StreamWriter);
                SW = File.CreateText(Server.MapPath("api\\xmlRec" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
                SW.WriteLine(xmlData);
                SW.Close();
        
            }
            catch (System.Net.WebException exp)
            {

                System.IO.StreamWriter SW = default(System.IO.StreamWriter);
                SW = File.CreateText(Server.MapPath("api\\error" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
                SW.WriteLine(exp.ToString());
                SW.Close();
            }
            

        }
        private string SendSMS(string strMsg, string MobNo)
        {
            string SmsStatusMsg = string.Empty;
            try
            {
                string username = "cdscr";
                string password = "CDSCR1@23";
                string svcCredentials = Base64Encode(username + ":" + password);
                var client = new RestClient("http://smsapi.webcomkenya.com/sms/1/text/single");
                var request = new RestRequest("", Method.POST);
                request.AddHeader("Content-Type", "application/json; encoding='utf-8'");
                request.AddHeader("Accept", "application/json; encoding='utf-8'");
                request.AddHeader("Authorization", "Basic " + svcCredentials);
                request.AddParameter("from","CDSCR");
                request.AddParameter("to", MobNo);
                request.AddParameter("text",strMsg);
                IRestResponse response = client.Execute(request);

                var validate = response.Content;

                SmsStatusMsg = validate;

            }
            catch (Exception f)
            {

                SmsStatusMsg = f.Message;
            }
            return SmsStatusMsg;

        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public void savelog(string s)
        {
            try
            {
                dynamic json = JObject.Parse(s);
                // dynamic o = JsonConvert.DeserializeObject(json.ToString());
               dynamic tra = json.Body.stkCallback.CheckoutRequestID;
                dynamic re= json.Body.stkCallback.ResultDesc;
                string transid = tra.ToString();
                string resp = re.ToString();

                string pay = "";
                string went = "";
              
                 
                        if (resp.Contains("successfully")==true)
                        {
                            pay = "PAID";
                            went = "SUCCESS";
                        }
                        else
                        {
                            pay = "NPAID";
                            went = "NSUCCESS";
                        }

                //Update Payment table
                var uppu = BackC.Payments.Where(a => a.CheckOutID==transid.Trim()).FirstOrDefault();
                var myyr = BackC.Payments.Find(uppu.ID);
                myyr.PaymentStatus = went;
                myyr.status = went;
                BackC.Payments.AddOrUpdate(myyr);
                BackC.SaveChanges();
                //insert into withdrawals

                //Update Safari
                var upp = BackC.SafariTrans.Where(a => a.CheckoutRequestID.Trim() == transid.Trim()).FirstOrDefault();
                var myy = BackC.SafariTrans.Find(upp.SafariTransID);
                myy.CallBack =s;
                BackC.SafariTrans.AddOrUpdate(myy);
                BackC.SaveChanges();

                string receiptNumber = "Rec_" + myyr.CustomerNumber + DateTime.Now.ToString("ddMMyyhhmmsss");
                //check if payment falls in loans
                if (went=="SUCCESS")
                {

             
               if (myyr.PaymentType.Contains("Loan")==true)
               {
                   var loanid = myyr.PaymentType.Split(',')[1];
                   BackC2.SaveAccountsTrxnsTempWithContra("Loans", "Loans",myyr.CustomerNumber,loanid, myyr.PaymentType + " " + DateTime.Now.ToString("dd MMMM yyyy"), 0, myyr.Amount,"601/3", true, "Share Ac 601/3", "Share Ac 601/3", "Share Ac 601/3", myyr.PaymentType, DateTime.Now.ToString(), myyr.CustomerNumber, receiptNumber, myyr.CustomerNumber);
                    BackC2.CommitTransaction(receiptNumber);

                   var payy = "Your loan repayment for "+myyr.PaymentType+" on "+ myyr.PaymentDate+" was successful. For an amount of  KES "+ Convert.ToDecimal(myyr.Amount).ToString("#,##.00");
                        payy = "Your payment with " + myyr.PaymentType + "  has been made on " + myyr.PaymentDate + " for the amount of KES " + Convert.ToDecimal(myyr.Amount).ToString("#,##.00") + ".";

                        string trues = SendSMS(payy,myyr.MobileNumber.Replace("+",""));
                   //mailReceipt(myyr.CustomerNumber, receiptNumber, emm(myyr.CustomerNumber), names(myyr.CustomerNumber));
                }
                    else
                {
                
                    BackC2.SaveAccountsTrxnsTempWithContra(myyr.PaymentType, myyr.PaymentType, myyr.CustomerNumber,myyr.PaymentType,myyr.PaymentType+" "+DateTime.Now.ToString("dd MMMM yyyy"),0,myyr.Amount, "601/2", true, "Savings Ac 601/2", "Savings Ac 601/2", "Savings Ac 601 / 2", myyr.PaymentType,DateTime.Now.ToString(),myyr.CustomerNumber, receiptNumber, myyr.CustomerNumber);
                    BackC2.CommitTransaction(receiptNumber);
                    var payy = "Your payment for " + myyr.PaymentType + " on " + myyr.PaymentDate + " was successful. For an amount of  KES " + Convert.ToDecimal(myyr.Amount).ToString("#,##.00");
                        payy = "Your payment with" + myyr.PaymentType + "  has been made on " + myyr.PaymentDate + " for the amount of KES " + Convert.ToDecimal(myyr.Amount).ToString("#,##.00") + ".";
                        string trues = SendSMS(payy, myyr.MobileNumber.Replace("+", ""));
                        //send receipt
                    mailReceipt(myyr.CustomerNumber,receiptNumber,emm(myyr.CustomerNumber),names(myyr.CustomerNumber));
                }

                }
            }
            catch (Exception f)
            {
                System.IO.StreamWriter SW = default(System.IO.StreamWriter);
                SW = File.CreateText(Server.MapPath("api\\error" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt"));
                SW.WriteLine(f.ToString());
                SW.Close();

            }
        }

        public void mailReceipt(string CustNo,string ReceiptNo,string Email,string Full)
        {
            var client = new RestClient("http://localhost/WesternJurist/Reporting/Default.aspx?CustNo="+ CustNo +"&ReceiptNo="+ ReceiptNo +"&Emails="+ Email +"&FullNames="+ Full +"");
            var request = new RestRequest("", Method.GET);
         IRestResponse response = client.Execute(request);

            var validate = response.Content;
        }
        public string names(string s)
        {
            var client = BackC2.CUSTOMER_DETAILS.ToList().Where(a => a.CUSTOMER_NUMBER == s).FirstOrDefault();
            return client.SURNAME + " " + client.FORENAMES;



        }


        public string emm(string s)
        {
            var client = BackC2.CUSTOMER_DETAILS.ToList().Where(a => a.CUSTOMER_NUMBER == s).FirstOrDefault();
            return client.Email;


        }
    }
}