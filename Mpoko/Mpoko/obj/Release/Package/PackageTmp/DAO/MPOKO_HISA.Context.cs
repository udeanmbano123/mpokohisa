﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Mpoko.DAO
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class MPOKO_HISAEntities : DbContext
    {
        public MPOKO_HISAEntities()
            : base("name=MPOKO_HISAEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Accounts_Transactions> Accounts_Transactions { get; set; }
        public virtual DbSet<Accounts_Transactions_Temp> Accounts_Transactions_Temp { get; set; }
        public virtual DbSet<APPLIED_LOANS> APPLIED_LOANS { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<SafariTran> SafariTrans { get; set; }
        public virtual DbSet<subscriber> subscribers { get; set; }
        public virtual DbSet<ESCROWCLIENT> ESCROWCLIENTS { get; set; }
    }
}
