﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class offerPrices
    {
        public string id { get; set; }
        public string price { get; set; }
        public string volume { get; set; }
    }
}