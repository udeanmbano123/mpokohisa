﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class MarketStatus
    {
        public string openTime { get; set; }
        public string closeTime { get; set; }
    }
}