﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class CashTrans
    {
        public long ID { get; set; }
        public string Description { get; set; }
        public string TransType { get; set; }
        [Column(TypeName = "money")]
        public Nullable<decimal> Amount { get; set; }
        public DateTime DateCreated { get; set; }
        public string TransStatus { get; set; }
        public string CDS_Number { get; set; }
    }

    public class CashTranss
    {
        public string id { get; set; }
        public string desc { get; set; }
        public string type { get; set; }
        [Column(TypeName = "money")]
        public Nullable<decimal> ammount { get; set; }
        public string date { get; set; }
        public string status { get; set; }
        //public string CDS_Number { get; set; }
    }
}