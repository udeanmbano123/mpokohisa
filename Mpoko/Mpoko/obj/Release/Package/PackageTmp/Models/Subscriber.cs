﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class Subscriber
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }

        public string CdsNumber { get; set; }
        
        public DateTime Date { get; set; }
    }
    public class SubscriberNew
    {
        public int id { get; set; }
        public string email { get; set; }
        public string username { get; set; }
        public string cds { get; set; }
        public string broker { get; set; }
        public bool Active { get; set; }


    }

}