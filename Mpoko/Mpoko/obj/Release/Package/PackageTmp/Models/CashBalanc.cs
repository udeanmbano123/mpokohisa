﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class CashBalanc
    {
        public string id { get; set; }
        public string MyPotValue { get; set; }
        public string MyProfitLoss { get; set; }
        public string CashBal { get; set; }
        public string VirtCashBal { get; set; }
        public string totalAccount { get; set; }
    }
}