﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Helpers;
//using System.Net.Mail;
using System.Web.Mvc;

using Mpoko.Models;
using Subscriber = Mpoko.Models.Subscriber;
using System.Security.Cryptography;
using System.Globalization;
using System.Net.Http;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json;
using System.Xml;
using RestSharp;
using RestSharp.Authenticators;

namespace Mpoko.Controllers
{
    public class SubscriberController : Controller
    {
        readonly cdscDbContext _cdscDbContext = new cdscDbContext();
        readonly CdDataContext _cdDataContext = new CdDataContext();
        readonly AtsDbContext _AtsDbContext = new AtsDbContext();
        string connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
        string connectionStringATS = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
        private static readonly HttpClient client = new HttpClient();

        long LongRandom(long min, long max, Random rand)
        {
            byte[] buf = new byte[8];
            rand.NextBytes(buf);
            long longRand = BitConverter.ToInt64(buf, 0);
            return (Math.Abs(longRand % (max - min)) + min);
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("SMS")]
        [AllowAnonymous]
        public dynamic meu(string message, string mobile, string sender)
        {
            string username = "cdscr";
            string password = "CDSCR1@23";
            string svcCredentials = Base64Encode(username + ":" + password);
            var client = new RestClient("http://smsapi.webcomkenya.com/sms/1/text/single");
            var request = new RestRequest("", Method.POST);
            request.AddHeader("Content-Type", "application/json; encoding='utf-8'");
            request.AddHeader("Accept", "application/json; encoding='utf-8'");
            request.AddHeader("Authorization", "Basic " + svcCredentials);
            request.AddParameter("from", sender);
            request.AddParameter("to", mobile);
            request.AddParameter("text", message);
            IRestResponse response = client.Execute(request);

            var validate = response.Content;

            return validate;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("SMSM/{message}/{mobile}/{sender}")]
        [AllowAnonymous]
        public dynamic meur(string message, string mobile, string sender)
        {
            message = decodephr(message);
            mobile = decodephr(mobile);
            sender = decodephr(sender);
            string username = "cdscr";
            string password = "CDSCR1@23";
            string svcCredentials = Base64Encode(username + ":" + password);
            var client = new RestClient("http://smsapi.webcomkenya.com/sms/1/text/single");
            var request = new RestRequest("", Method.POST);
            request.AddHeader("Content-Type", "application/json; encoding='utf-8'");
            request.AddHeader("Accept", "application/json; encoding='utf-8'");
            request.AddHeader("Authorization", "Basic " + svcCredentials);
            request.AddParameter("from", sender);
            request.AddParameter("to", mobile);
            request.AddParameter("text", message);
            IRestResponse response = client.Execute(request);

            var validate = response.Content;

            return validate;
        }
        public string decodephr(string s)
        {
            string str = "";
            try
            {
                s = s.Replace('-', '+');
                s = s.Replace('_', '/');
                byte[] bytes = Convert.FromBase64String(s);
                str = Encoding.UTF8.GetString(bytes);
            }
            catch (Exception)
            {

                str = "0";
            }
            return str;
        }

        public JsonResult MarketWatch()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql =
                "SELECT cp.[COMPANY] as Company, [SHARESINISSUE]  as SharesInIssue, [ClosingPrice] as ClosingPrice, [BestPrice] as CurrentPrice, UPPER(pc.fnam) as CompanyName FROM[testcds_ROUTER].[dbo].[CompanyPrices] cp join para_company pc on pc.Company = cp.COMPANY";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No data was found", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult MarketWatchZSE()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcherZSE>();

            var sql =
                "SELECT * FROM ZSE_market_data";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcherZSE recordSummary = new MarketWatcherZSE();
                    recordSummary.Ticker = rdr["Ticker"].ToString();
                    recordSummary.ISIN = rdr["ISIN"].ToString();
                    recordSummary.Best_Ask = rdr["Best_Ask"].ToString();
                    recordSummary.Best_bid = rdr["Best_bid"].ToString();
                    recordSummary.Current_price = rdr["Current_price"].ToString();
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No data was found", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetKYC(string cds_number)
        {
            var list = new List<ESCROWCLIENTS>();

            var sql =
                " select * FROM [Mpoko_HISA].[dbo].[ESCROWCLIENTS] where CLIENT_PREFIX = '" + cds_number + "' ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ESCROWCLIENTS recordSummary = new ESCROWCLIENTS();
                    recordSummary.ACCOUNT = rdr["ACCOUNT"].ToString();
                    recordSummary.ADDRESS = rdr["ADDRESS"].ToString();
                    recordSummary.BANK = rdr["BANK"].ToString();
                    recordSummary.BRANCH = rdr["BRANCH"].ToString();
                    recordSummary.CLIENT_PREFIX = rdr["CLIENT_PREFIX"].ToString();
                    recordSummary.CLIENT_SUFFIX = rdr["CLIENT_SUFFIX"].ToString();
                    recordSummary.CLIENT_TYPE = rdr["CLIENT_TYPE"].ToString();
                    recordSummary.DATE_CHANGED = rdr["DATE_CHANGED"].ToString();
                    recordSummary.DATE_OF_REGISTRATION = rdr["DATE_OF_REGISTRATION"].ToString();
                    recordSummary.e_MAIL = rdr["e_MAIL"].ToString();
                    recordSummary.MEMBER_CODE = rdr["MEMBER_CODE"].ToString();
                    recordSummary.MEMBER_TYPE = rdr["MEMBER_TYPE"].ToString();
                    recordSummary.OTHER_NAMES = rdr["OTHER_NAMES"].ToString();
                    recordSummary.SURNAME = rdr["SURNAME"].ToString();
                    recordSummary.TELEPHONE = rdr["TELEPHONE"].ToString();
                    list.Add(recordSummary);
                }
            }

            if (list.Any())
            {
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAppliedLoans(string cds_number)
        {
            var list = new List<APPLIED_LOANS>();

            var sql =
                "select * FROM [CEDA_CMS].[dbo].[QUEST_APPLICATION] where customer_number ='" + cds_number + "' order by id desc";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    APPLIED_LOANS recordSummary = new APPLIED_LOANS();
                    recordSummary.id = int.Parse(rdr["id"].ToString());
                    recordSummary.counter = rdr["customer_number"].ToString();
                    recordSummary.shares_loaned = decimal.Parse(rdr["fin_amt"].ToString()).ToString();
                    recordSummary.share_price = decimal.Parse(rdr["fin_amt"].ToString()).ToString();
                    recordSummary.loan_amount = decimal.Parse(rdr["fin_amt"].ToString()).ToString();
                    recordSummary.interest = decimal.Parse(rdr["int_rate"].ToString()).ToString();
                    recordSummary.repayment_period = rdr["period"].ToString();
                    recordSummary.created_date = DateTime.Parse(rdr["CREATED_DATE"].ToString()).ToString("dd MMM yyyy HH:mm");
                    recordSummary.first_repay_date = DateTime.Parse(rdr["FIN_REPAY_DATE"].ToString()).ToString("dd MMM yyyy");
                    recordSummary.status = rdr["decision"].ToString();
                    list.Add(recordSummary);
                }
            }

            if (list.Any())
            {
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetLoansForRepayment(string cds_number)
        {
            var list = new List<APPLIED_LOANS>();

            var sql =
                "select * FROM [CEDA_CMS].[dbo].[QUEST_APPLICATION] where customer_number ='" + cds_number + "' and Decision = 'DISBURSED' order by id desc";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    APPLIED_LOANS recordSummary = new APPLIED_LOANS
                    {
                        id = int.Parse(rdr["id"].ToString()),
                        counter = rdr["customer_number"].ToString(),
                        shares_loaned = decimal.Parse(rdr["fin_amt"].ToString()).ToString(),
                        share_price = decimal.Parse(rdr["fin_amt"].ToString()).ToString(),
                        loan_amount = decimal.Parse(rdr["fin_amt"].ToString()).ToString(),
                        interest = decimal.Parse(rdr["int_rate"].ToString()).ToString(),
                        repayment_period = rdr["period"].ToString(),
                        created_date = DateTime.Parse(rdr["CREATED_DATE"].ToString()).ToString("dd MMM yyyy HH:mm"),
                        first_repay_date = DateTime.Parse(rdr["FIN_REPAY_DATE"].ToString()).ToString("dd MMM yyyy"),
                        status = rdr["decision"].ToString()
                    };
                    list.Add(recordSummary);
                }
            }

            if (list.Any())
            {
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetMyCollateral(string cds_number)
        {
            List<DATASET_HOLDINGS> list = new List<DATASET_HOLDINGS>();

            cds_number = cds_number.Trim();

            var sql =
                "select CLIENT_PREFIX,CLIENT_SUFFIX,MEMBER_CODE,MEMBER_TYPE,SHORT_NAME,ISSUER_CODE,MAIN_TYPE, SUB_TYPE,qnty_held,DATE_LAST_TRANSACTION, sec_ACC_TYPE from DATASET_HOLDINGS where CLIENT_PREFIX = '" + cds_number + "'";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    DATASET_HOLDINGS recordSummary = new DATASET_HOLDINGS();
                    recordSummary.MEMBER_CODE = rdr["MEMBER_CODE"].ToString();
                    recordSummary.SHORT_NAME = rdr["SHORT_NAME"].ToString();
                    recordSummary.ISSUER_CODE = rdr["ISSUER_CODE"].ToString();
                    recordSummary.qnty_held = rdr["qnty_held"].ToString();
                    recordSummary.DATE_LAST_TRANSACTION = rdr["DATE_LAST_TRANSACTION"].ToString();
                    list.Add(recordSummary);
                }
            }

            if (list.Any())
            {
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult versionControl()
        {
            List<AppVersion> version = new List<AppVersion>
            {
                new AppVersion() { appVersion = "1.0.0" }
            };
            return Json(version, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Register(string username, string cds_number, string phone, string email, string gender, string dob, string password, string smsopt)
        {
            var result = "0";

            Boolean textsopt = false;

            cds_number = cds_number.Trim();

            if (smsopt.Equals("true"))
            {
                textsopt = true;
            }


            string sqlCheckUser = " select count(*) FROM [subscribers] where cds_number = '" + cds_number + "'";

            SqlConnection connection = new SqlConnection(connectionString);

            using (SqlCommand sqlCommand = new SqlCommand(sqlCheckUser, connection))
            {
                connection.Open();

                if (sqlCommand.ExecuteScalar() == null)
                {
                    result = "Error occured in registering";
                }
                else
                {

                    int? count = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    if (count > 0)
                    {
                        result = "CDS Number has already been registered";
                    }
                    else
                    {

                        if (CheckUserRecord(cds_number, username).Equals("1"))
                        {
                            string sqlx = "insert into subscribers(username, cds_number, phone, email, dob, password, texts_opt, gender, created_date) values('" + username + "', '" + cds_number + "', '" + phone + "', '" + email + "', '" + dob + "', '" + password + "', '" + textsopt + "', '" + gender + "' ,GETDATE())";


                            using (SqlConnection connectionsx = new SqlConnection(connectionString))
                            {
                                SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                                connectionsx.Open();
                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteNonQuery();

                            }

                            result = "Account successfully created";
                        }
                        else
                        {
                            result = "Username not found in KYC records";
                        }


                    }


                }

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /*public JsonResult Register(string username, string cds_number, string phone, string email, string gender, string dob, string password, string smsopt)
        {
            var result = "0";

            Boolean textsopt = false;

            cds_number = cds_number.Trim();

            if (smsopt.Equals("true"))
            {
                textsopt = true;
            }


            string sqlCheckUser = " select count(*) FROM [subscribers] where cds_number = '" + cds_number + "'";

            SqlConnection connection = new SqlConnection(connectionString);

            using (SqlCommand sqlCommand = new SqlCommand(sqlCheckUser, connection))
            {
                connection.Open();

                if (sqlCommand.ExecuteScalar() == null)
                {
                    result = "Error occured in registering";
                }
                else
                {

                    int? count = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    if (count > 0)
                    {
                        result = "CDS Number has already been registered";
                    }
                    else
                    {

                        if (CheckUserRecord(cds_number, username).Equals("1"))
                        {
                            string sqlx = "insert into subscribers(username, cds_number, phone, email, dob, password, texts_opt, gender, created_date) values('" + username + "', '" + cds_number + "', '" + phone + "', '" + email + "', '" + dob + "', '" + password + "', '" + textsopt + "', '" + gender + "' ,GETDATE())";


                            using (SqlConnection connectionsx = new SqlConnection(connectionString))
                            {
                                SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                                connectionsx.Open();
                                cmd.CommandType = CommandType.Text;
                                cmd.ExecuteNonQuery();

                            }

                            result = "Account successfully created";
                        }
                        else
                        {
                            result = CheckUserRecord(cds_number, username);
                        }


                    }


                }

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }*/

        public JsonResult ApplyLoan(string cds_number, string counter,
            string shares_loaned, string share_price, string loan_amount,
            string repayment_period, string interest, string debit,
            string credit, string repay_intervals, string source_of_repay, string current_residence,
            string current_borrowings, string main_income, string other_income,
            string employment_creation, string experience, string resource_availability,
            string monthly_income)
        {
            var result = "0";

            string sqlx = "insert into APPLIED_LOANS(cds_number, counter, shares_loaned, share_price, loan_amount, repayment_period, interest, debit, credit, repay_intervals, source_of_repay,current_residence, current_borrowings, main_income, other_income, employment_creation, experience, resource_availability,application_date, first_repay_date,monthly_income) values('" + cds_number + "', '" + counter + "', '" + shares_loaned + "', '" + share_price + "', '" + loan_amount + "', '" + repayment_period + "', '" + interest + "', '" + debit + "', '" + credit + "', '" + repay_intervals + "', '" + source_of_repay + "','" + current_residence + "', '" + current_borrowings + "', '" + main_income + "', '" + other_income + "', '" + employment_creation + "', '" + experience + "', '" + resource_availability + "', getdate(), DATEADD(month, 1, getdate()),'" + monthly_income + "')";

            using (SqlConnection connectionsx = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                connectionsx.Open();
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();

            }

            result = "Loan application submitted.";


            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string SendSMSA(string message, string mobile)
        {
            string SmsStatusMsg = string.Empty;
            try
            {
                string username = "cdscr";
                string password = "CDSCR1@23";
                string svcCredentials = Base64Encoding(username + ":" + password);
                var client = new RestClient("http://smsapi.webcomkenya.com/sms/1/text/single");
                var request = new RestRequest("", Method.POST);
                request.AddHeader("Content-Type", "application/json; encoding='utf-8'");
                request.AddHeader("Accept", "application/json; encoding='utf-8'");
                request.AddHeader("Authorization", "Basic " + svcCredentials);
                request.AddParameter("from", "CDSCR");
                request.AddParameter("to", mobile);
                request.AddParameter("text", message);
                IRestResponse response = client.Execute(request);

                var validate = response.Content;
                SmsStatusMsg = validate;
            }
            catch (Exception f)
            {

                SmsStatusMsg = f.Message;
            }
            return SmsStatusMsg;

        }

        public static string Base64Encoding(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }


        public string CheckUserRecord(string cds_number, string id_number)
        {
            string sqlCheckUser = "select count(*) FROM [Mpoko_HISA].[dbo].[ESCROWCLIENTS] where client_prefix = '" + cds_number + "'  and ACCOUNT = '" + id_number + "' or e_MAIL = '" + id_number + "'";

            SqlConnection connection = new SqlConnection(connectionString);

            string result = "0";

            using (SqlCommand sqlCommand = new SqlCommand(sqlCheckUser, connection))
            {
                connection.Open();

                if (sqlCommand.ExecuteScalar() == null)
                {
                    result = "9";
                }
                else
                {

                    int? count = Convert.ToInt32(sqlCommand.ExecuteScalar());

                    if (count > 0)
                    {
                        result = "1";
                    }

                }
            }

            return result;
        }

        public JsonResult PayLoan(string phone, string email, string amount)
        {
            string timestamp = DateTime.Parse(DateTime.Now.ToString()).ToString("yyyyMMddHHssSS");
            string result = null;

            WebRequest request = WebRequest.Create("http://192.168.3.8:8384/EscrowServiceMH/rest/MHService/lipa/" + phone + "/" + email + "/" + amount + "/" + timestamp);
            request.Method = "POST";
            WebResponse response = request.GetResponse();
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                result = reader.ReadToEnd();
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult marketStatus(String market)
        {
            if (market == "ZSE")
            {
                List<MarketStatus> version = new List<MarketStatus>
            {
                new MarketStatus(){openTime ="1000", closeTime ="1500"}
            };
                return Json(version, JsonRequestBehavior.AllowGet);

            }
            else if (market == "FINSEC")
            {
                List<MarketStatus> version = new List<MarketStatus>
            {
                new MarketStatus(){openTime ="900", closeTime ="1630"}
            };
                return Json(version, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return null;
            }

        }


        public JsonResult AuthUser(string username, string password)
        {
            var list = new List<ESCROWCLIENTS>();

            username = username.Trim();
            password = password.Trim();

            var sql = "select a.*, s.* from ESCROWCLIENTS a join subscribers s on a.CLIENT_PREFIX = s.cds_number where s.username = '" + username + "' and s.password = '" + password + "'";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ESCROWCLIENTS recordSummary = new ESCROWCLIENTS
                    {
                        ACCOUNT = rdr["ACCOUNT"].ToString(),
                        ADDRESS = rdr["ADDRESS"].ToString(),
                        BANK = rdr["BANK"].ToString(),
                        BRANCH = rdr["BRANCH"].ToString(),
                        CLIENT_PREFIX = rdr["CLIENT_PREFIX"].ToString(),
                        CLIENT_SUFFIX = rdr["CLIENT_SUFFIX"].ToString(),
                        CLIENT_TYPE = rdr["CLIENT_TYPE"].ToString(),
                        DATE_CHANGED = rdr["DATE_CHANGED"].ToString(),
                        DATE_OF_REGISTRATION = rdr["DATE_OF_REGISTRATION"].ToString(),
                        e_MAIL = rdr["e_MAIL"].ToString(),
                        MEMBER_CODE = rdr["MEMBER_CODE"].ToString(),
                        MEMBER_TYPE = rdr["MEMBER_TYPE"].ToString(),
                        OTHER_NAMES = rdr["OTHER_NAMES"].ToString(),
                        SURNAME = rdr["SURNAME"].ToString(),
                        TELEPHONE = rdr["TELEPHONE"].ToString(),
                        USERNAME = rdr["username"].ToString(),
                        DOB = rdr["dob"].ToString()
                    };
                    list.Add(recordSummary);
                }
            }

            if (list.Any())
            {
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult checkUser(string mobile, string password)
        {

            string connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;

            string hashPass = Base64Encode(password);

            string sqlCheckUser = "SELECT * FROM[BrokerUSSD].[dbo].[UssdRegs] where Msisdn = '" + mobile + "' and Pin = '" + hashPass + "'";

            SqlConnection connection = new SqlConnection(connectionString);

            string result = hashPass;

            using (SqlCommand sqlCommand = new SqlCommand(sqlCheckUser, connection))
            {
                connection.Open();

                if (sqlCommand.ExecuteScalar() == null)
                {
                    result = hashPass;
                }
                else
                {

                    int userCount = (int)sqlCommand.ExecuteScalar();

                    int count = (int)sqlCommand.ExecuteScalar();
                    if (count > 0)
                    {
                        result = "Found";
                    }

                }

                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public JsonResult myportfolio()
        {

            string result = null;

            WebRequest request = WebRequest.Create("http://172.20.20.10:1577/Safariweb/EscrowSoapWebService.asmx/balance?TelephoneEnqure=254737691259&company=KE5000006766");
            request.Method = "GET";
            WebResponse response = request.GetResponse();
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                result = reader.ReadToEnd();
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);

            string json = JsonConvert.SerializeXmlNode(doc);

            return Json(json, JsonRequestBehavior.AllowGet);
        }


        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        public JsonResult Balance(string cdsNumber = null)
        {

            var sums = _cdDataContext.trans.Join(_cdDataContext.para_company, c => c.Company, d => d.Company,
                (c, d) => new
                {
                    Company = d.Fnam,
                    CompanyCode = c.Company,
                    Shares = c.Shares,
                    CdsNumber = c.CDS_Number
                })
             .Where(d => d.CdsNumber == cdsNumber)
             .GroupBy(d => d.Company)
             .Select(g =>
                 new
                 {
                     Name = g.Select(s => s.Company).FirstOrDefault(),
                     ISIN = g.Select(s => s.CompanyCode).FirstOrDefault(),
                     Shares = g.Sum(s => s.Shares),
                 });

            return Json(sums, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Balances(string cdsNumber = null)
        {

            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            var CompShares = new List<Balances>();

            var sql = "select tm.*,pc.Fnam from (select tx.Company,tx.CDS_Number,sum(tx.Shares) as totShares from trans tx group by tx.Company,tx.CDS_Number) tm left Join para_company pc on tm.Company = pc.Company  where tm.CDS_Number = '" + cdsNumber + "' and tm.totShares>0";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Balances recordSummary = new Balances();
                    recordSummary.Name = rdr["Fnam"].ToString();
                    recordSummary.ISIN = rdr["Company"].ToString();
                    recordSummary.Shares = long.Parse(rdr["totShares"].ToString());
                    CompShares.Add(recordSummary);
                }
            }

            if (CompShares.Any())
            {
                return Json(CompShares, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No data was found", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SecMovement(string cdsNumber = null)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            var marketWatcher = new List<PortfolioMovement>();

            var sql = "select tx.Shares as Shares, tx.Date_Created as Date, tx.Company, pc.Fnam as CompanyName, ISNULL(tx.Instrument, 'EQUITY') as Instrument from trans tx    Join para_company pc on tx.Company = pc.Company  where CDS_Number = '" + cdsNumber + "' order by tx.Date_created asc";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    PortfolioMovement recordSummary = new PortfolioMovement();
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    recordSummary.Shares = rdr["Shares"].ToString();
                    recordSummary.Date = DateTime.Parse(rdr["Date"].ToString()).ToShortDateString();
                    recordSummary.Instrument = rdr["Instrument"].ToString();
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No data was found", JsonRequestBehavior.AllowGet);
            }
        }

        //ACCOUNT CREATION PARAMETERES
        public JsonResult GetCompanies(string company = null)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            var marketWatcher = new List<GetBroker>();

            var sql = "select distinct UPPER(Company_name) as Name, [Company_Code] as Code FROM [CDS_ROUTER].[dbo].[Client_Companies]";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    GetBroker recordSummary = new GetBroker();
                    recordSummary.Name = rdr["Name"].ToString();
                    recordSummary.Code = rdr["Code"].ToString();

                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No data was found", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetBrokersLimits(string company = null)
        {
            var getBrk =
                "  select Company_Code as Code, upper(Company_name) as Name  FROM [CDS_ROUTER].[dbo].[Client_Companies] where Company_type='BROKER'";


            var listOfBrokers = new List<BrokerLimits>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    BrokerLimits broker = new BrokerLimits();
                    broker.Code = rdr["Code"].ToString();
                    broker.Name = rdr["Name"].ToString();
                    broker.threshold = rdr["min_value_threshold"].ToString();
                    listOfBrokers.Add(broker);
                }

                try
                {
                    var jsonResult = Json(listOfBrokers, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception)
                {
                    return Json("Data set too large", JsonRequestBehavior.AllowGet);
                }
            }
        }


        public JsonResult GetCompany(string exchange, string range, string company = null)
        {
            //Selecting distinct
            var companies = _AtsDbContext.para_company.Where(x => x.exchange.Contains(exchange)).GroupBy(x => x.fnam).Select(x => x.FirstOrDefault()).Select(c =>
                new
                {
                    Id = c.ID,
                    Name = c.fnam,
                    c.InitialPrice
                });
            return Json(companies, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MobileMoney(string company = null)
        {
            var mobileMoney = _cdDataContext.para_mobile_money.GroupBy(x => x.Mobile_money_name).Select(x => x.FirstOrDefault()).Select(c => new
            {
                Id = c.ID,
                Name = c.Mobile_money_name
            });
            return Json(mobileMoney, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Banks(string company = null)
        {
            var getBanks = _cdDataContext.para_bank.GroupBy(x => x.bank_name).Select(x => x.FirstOrDefault()).Select(c => new
            {
                Id = c.bank,
                Code = c.bank,
                Name = c.bank_name
            });
            return Json(getBanks, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Branch(string bank = null)
        {
            var getBranch = _cdDataContext.para_branch.GroupBy(x => x.branch_name).Select(x => x.FirstOrDefault()).Where(x => x.bank == bank).Select(c => new
            {
                Name = c.branch_name
            });
            return Json(getBranch, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetBranchFromFullName(string bank_name)
        {
            var sql =
                "  Select t.bank_name , ut.branch_name FROM para_bank t, para_branch ut WHERE t.bank = ut.bank and t.bank_name = '" +
                bank_name + "'";
            var marketNews = new List<Branches>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Branches recordSummary = new Branches();
                    recordSummary.Name = rdr["branch_name"].ToString();

                    marketNews.Add(recordSummary);
                }
            }


            if (marketNews.Any())
            {
                return Json(marketNews, JsonRequestBehavior.AllowGet);
            }
            return Json("HEAD", JsonRequestBehavior.AllowGet);

        }



        public JsonResult Prices(string oldpass)
        {

            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql =
                "SELECT cp.[COMPANY] as Company ,[SHARESINISSUE]  as SharesInIssue ,[ClosingPrice] as ClosingPrice ,[BestPrice] as CurrentPrice ,UPPER(pc.fnam) as CompanyName ,convert (decimal(10, 2),  (([BestPrice] - [ClosingPrice])/[ClosingPrice])*100) as Change FROM [testcds_ROUTER].[dbo].[CompanyPrices] cp join para_company pc on pc.Company = cp.COMPANY ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.Change = decimal.Parse(rdr["Change"].ToString());

                    recordSummary.ChangeString = decimal.Parse(rdr["Change"].ToString()).ToString("##.000");
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    recordSummary.Exchange = "FINSEC";
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EscrosharePrices()
        {

            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql =
                "select a.Company as Company,a.PricePerShare as SharesInIssue,a.PricePerShare as ClosingPrice,a.PricePerShare as CurrentPrice,UPPER(c.fnam) as CompanyName,convert (decimal(10, 2),a.PercentageChange) as Change  from PortFolio a join para_company c on a.Company=c.company where a.DateToday=(select top 1 b.DateToday from PortFolio b order by b.id desc)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.Change = decimal.Parse(rdr["Change"].ToString());

                    recordSummary.ChangeString = decimal.Parse(rdr["Change"].ToString()).ToString("##.000");
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    recordSummary.Exchange = "ZSE";
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AllPrices()
        {

            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var connectionString2 = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql =
                "select a.Company as Company,a.PricePerShare as SharesInIssue,a.PricePerShare as ClosingPrice,a.PricePerShare as CurrentPrice,UPPER(c.fnam) as CompanyName,convert (decimal(10, 2),a.PercentageChange) as Change  from PortFolio a join para_company c on a.Company=c.company where a.DateToday=(select top 1 b.DateToday from PortFolio b order by b.id desc)";
            var sql2 =
                "SELECT cp.[COMPANY] as Company ,[SHARESINISSUE]  as SharesInIssue ,[ClosingPrice] as ClosingPrice ,[BestPrice] as CurrentPrice ,UPPER(pc.fnam) as CompanyName ,convert (decimal(10, 2),  (([BestPrice] - [ClosingPrice])/[ClosingPrice])*100) as Change FROM [testcds_ROUTER].[dbo].[CompanyPrices] cp join para_company pc on pc.Company = cp.COMPANY ";


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.Change = decimal.Parse(rdr["Change"].ToString());

                    recordSummary.ChangeString = decimal.Parse(rdr["Change"].ToString()).ToString("##.000");
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    recordSummary.Exchange = "ZSE";
                    marketWatcher.Add(recordSummary);
                }
            }
            //append

            using (SqlConnection connection2 = new SqlConnection(connectionString2))
            {
                SqlCommand cmd2 = new SqlCommand(sql2, connection2);
                cmd2.CommandType = CommandType.Text;
                connection2.Open();
                SqlDataReader rdr2 = cmd2.ExecuteReader();
                while (rdr2.Read())
                {
                    MarketWatcher recordSummary2 = new MarketWatcher();
                    recordSummary2.Company = rdr2["Company"].ToString();
                    recordSummary2.SharesInIssue = rdr2["SharesInIssue"].ToString();
                    recordSummary2.ClosingPrice = Math.Round(decimal.Parse(rdr2["ClosingPrice"].ToString()), 4);
                    recordSummary2.CurrentPrice = Math.Round(decimal.Parse(rdr2["CurrentPrice"].ToString()), 4);
                    recordSummary2.Change = decimal.Parse(rdr2["Change"].ToString());

                    recordSummary2.ChangeString = decimal.Parse(rdr2["Change"].ToString()).ToString("##.000");
                    recordSummary2.CompanyName = rdr2["CompanyName"].ToString();
                    recordSummary2.Exchange = "FINSEC";
                    marketWatcher.Add(recordSummary2);
                }
            }
            //append

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Top_Gainers()
        {

            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var connectionString2 = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql =
                "select a.Company as Company,a.PricePerShare as SharesInIssue,a.PricePerShare as ClosingPrice,a.PricePerShare as CurrentPrice,UPPER(c.fnam) as CompanyName,convert (decimal(10, 2),a.PercentageChange) as Change  from PortFolio a join para_company c on a.Company=c.company where a.DateToday=(select top 1 b.DateToday from PortFolio b order by b.id desc) and a.PercentageChange>0 order by a.PercentageChange desc";
            var sql2 =
                "SELECT cp.[COMPANY] as Company ,[SHARESINISSUE]  as SharesInIssue ,[ClosingPrice] as ClosingPrice ,[BestPrice] as CurrentPrice ,UPPER(pc.fnam) as CompanyName ,convert (decimal(10, 2),  (([BestPrice] - [ClosingPrice])/[ClosingPrice])*100) as Change FROM [testcds_ROUTER].[dbo].[CompanyPrices] cp join para_company pc on pc.Company = cp.COMPANY  WHERE cp.[BestPrice] - cp.[ClosingPrice]>0 order by cp.[BestPrice] - cp.[ClosingPrice] desc";


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.Change = decimal.Parse(rdr["Change"].ToString());

                    recordSummary.ChangeString = decimal.Parse(rdr["Change"].ToString()).ToString("##.000");
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    recordSummary.Exchange = "ZSE";
                    marketWatcher.Add(recordSummary);
                }
            }
            //append

            using (SqlConnection connection2 = new SqlConnection(connectionString2))
            {
                SqlCommand cmd2 = new SqlCommand(sql2, connection2);
                cmd2.CommandType = CommandType.Text;
                connection2.Open();
                SqlDataReader rdr2 = cmd2.ExecuteReader();
                while (rdr2.Read())
                {
                    MarketWatcher recordSummary2 = new MarketWatcher();
                    recordSummary2.Company = rdr2["Company"].ToString();
                    recordSummary2.SharesInIssue = rdr2["SharesInIssue"].ToString();
                    recordSummary2.ClosingPrice = Math.Round(decimal.Parse(rdr2["ClosingPrice"].ToString()), 4);
                    recordSummary2.CurrentPrice = Math.Round(decimal.Parse(rdr2["CurrentPrice"].ToString()), 4);
                    recordSummary2.Change = decimal.Parse(rdr2["Change"].ToString());

                    recordSummary2.ChangeString = decimal.Parse(rdr2["Change"].ToString()).ToString("##.000");
                    recordSummary2.CompanyName = rdr2["CompanyName"].ToString();
                    recordSummary2.Exchange = "FINSEC";
                    marketWatcher.Add(recordSummary2);
                }
            }
            //append

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Top_Losers()
        {

            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var connectionString2 = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql =
                "select a.Company as Company,a.PricePerShare as SharesInIssue,a.PricePerShare as ClosingPrice,a.PricePerShare as CurrentPrice,UPPER(c.fnam) as CompanyName,convert (decimal(10, 2),a.PercentageChange) as Change  from PortFolio a join para_company c on a.Company=c.company where a.DateToday=(select top 1 b.DateToday from PortFolio b order by b.id desc) and a.PercentageChange<0 order by a.PercentageChange asc";
            var sql2 =
                "SELECT cp.[COMPANY] as Company ,[SHARESINISSUE]  as SharesInIssue ,[ClosingPrice] as ClosingPrice ,[BestPrice] as CurrentPrice ,UPPER(pc.fnam) as CompanyName ,convert (decimal(10, 2),  (([BestPrice] - [ClosingPrice])/[ClosingPrice])*100) as Change FROM [testcds_ROUTER].[dbo].[CompanyPrices] cp join para_company pc on pc.Company = cp.COMPANY  WHERE cp.[BestPrice] - cp.[ClosingPrice]<0 order by cp.[BestPrice] - cp.[ClosingPrice] asc";


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.Change = decimal.Parse(rdr["Change"].ToString());

                    recordSummary.ChangeString = decimal.Parse(rdr["Change"].ToString()).ToString("##.000");
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    recordSummary.Exchange = "ZSE";
                    marketWatcher.Add(recordSummary);
                }
            }
            //append

            using (SqlConnection connection2 = new SqlConnection(connectionString2))
            {
                SqlCommand cmd2 = new SqlCommand(sql2, connection2);
                cmd2.CommandType = CommandType.Text;
                connection2.Open();
                SqlDataReader rdr2 = cmd2.ExecuteReader();
                while (rdr2.Read())
                {
                    MarketWatcher recordSummary2 = new MarketWatcher();
                    recordSummary2.Company = rdr2["Company"].ToString();
                    recordSummary2.SharesInIssue = rdr2["SharesInIssue"].ToString();
                    recordSummary2.ClosingPrice = Math.Round(decimal.Parse(rdr2["ClosingPrice"].ToString()), 4);
                    recordSummary2.CurrentPrice = Math.Round(decimal.Parse(rdr2["CurrentPrice"].ToString()), 4);
                    recordSummary2.Change = decimal.Parse(rdr2["Change"].ToString());

                    recordSummary2.ChangeString = decimal.Parse(rdr2["Change"].ToString()).ToString("##.000");
                    recordSummary2.CompanyName = rdr2["CompanyName"].ToString();
                    recordSummary2.Exchange = "FINSEC";
                    marketWatcher.Add(recordSummary2);
                }
            }
            //append

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TopGainers(string oldpass)
        {

            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql =
                "SELECT cp.[COMPANY] as Company ,[SHARESINISSUE]  as SharesInIssue , [ClosingPrice] as ClosingPrice,[BestPrice] as CurrentPrice ,UPPER(pc.fnam) as CompanyName ,convert(decimal(10, 2), (([BestPrice] - [ClosingPrice]) /[ClosingPrice]) * 100) as Change FROM [CompanyPrices] cp join para_company pc on pc.Company = cp.COMPANY where (([BestPrice] - [ClosingPrice]) /[ClosingPrice]) * 100 > 0 order by Change desc  ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.Change = Math.Round(decimal.Parse(rdr["Change"].ToString()), 4);

                    recordSummary.ChangeString = decimal.Parse(rdr["Change"].ToString()).ToString("##.000");

                    float f = float.Parse(rdr["Change"].ToString());

                    recordSummary.ChangeString = f.ToString("##,####");


                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TopLosers(string oldpass)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatcher>();

            var sql = "SELECT top 10 cp.[COMPANY] as Company ,[SHARESINISSUE]  as SharesInIssue ,[ClosingPrice] as ClosingPrice ,[BestPrice] as CurrentPrice ,UPPER(pc.fnam) as CompanyName ,convert(decimal(10, 2),  (([BestPrice] - [ClosingPrice])/[ClosingPrice])*100) as Change FROM[testcds_ROUTER].[dbo].[CompanyPrices] cp join para_company pc on pc.Company = cp.COMPANY where (([BestPrice] - [ClosingPrice])/[ClosingPrice])*100 < 0 order by Change asc";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatcher recordSummary = new MarketWatcher();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.SharesInIssue = rdr["SharesInIssue"].ToString();
                    recordSummary.ClosingPrice = Math.Round(decimal.Parse(rdr["ClosingPrice"].ToString()), 4);
                    recordSummary.CurrentPrice = Math.Round(decimal.Parse(rdr["CurrentPrice"].ToString()), 4);
                    recordSummary.Change = decimal.Parse(decimal.Parse(rdr["Change"].ToString()).ToString("f4"));
                    recordSummary.CompanyName = rdr["CompanyName"].ToString();
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetNews()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var marketNews = new List<TheNews>();

            var sql =
                "SELECT ID,post_title,post_content,post_date,post_mime_type FROM News ORDER BY ID DESC";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    TheNews recordSummary = new TheNews();
                    recordSummary.id = long.Parse(rdr["ID"].ToString());
                    recordSummary.isImportant = true;
                    recordSummary.picture = "";
                    recordSummary.from = "FINSEC";
                    recordSummary.subject = rdr["post_title"].ToString();
                    recordSummary.message = rdr["post_content"].ToString();
                    recordSummary.timestamp = rdr["post_date"].ToString();
                    recordSummary.isRead = false;
                    marketNews.Add(recordSummary);
                }
            }

            if (marketNews.Any())
            {
                return Json(marketNews, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
        }

        public JsonResult MarketWatchs(string oldpass)
        {


            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatch>();

            var sql = "SELECT [company] as Company ,[Volume] as Volume ,[Bid] as Bid ,[Volume Sell] as VolumeSell ,[Ask] ,[Last Traded Price] as LastTradedPrice ,[Lastmatched] ,[lastvolume] ,[TotalVolume],[Turnover],[Open],[High],[Low],[Average Price] as AvgPrice,[Change],[percchange],[url],[url2]FROM[testcds_ROUTER].[dbo].[MarketWatch]";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatch recordSummary = new MarketWatch();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.Volume = rdr["Volume"].ToString();
                    recordSummary.Bid = rdr["Bid"].ToString();
                    recordSummary.VolumeSell = rdr["VolumeSell"].ToString();
                    recordSummary.Ask = rdr["Ask"].ToString();
                    recordSummary.LastTradedPrice = rdr["LastTradedPrice"].ToString();
                    recordSummary.Lastmatched = rdr["Lastmatched"].ToString();
                    recordSummary.Lastvolume = rdr["Lastvolume"].ToString();
                    recordSummary.TotalVolume = rdr["TotalVolume"].ToString();
                    recordSummary.Turnover = rdr["Turnover"].ToString();
                    recordSummary.Open = rdr["Open"].ToString();
                    recordSummary.High = rdr["High"].ToString();
                    recordSummary.Low = rdr["Low"].ToString();
                    recordSummary.AveragePrice = rdr["AvgPrice"].ToString();
                    recordSummary.Change = rdr["Change"].ToString();
                    recordSummary.Percchange = rdr["Percchange"].ToString();
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MarketWatchsComs(string name)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcher = new List<MarketWatch>();

            var sql = "SELECT [company] as Company, [Volume] as Volume ,[Bid] as Bid ,[Volume Sell] as VolumeSell ,[Ask] ,[Last Traded Price] as LastTradedPrice ,[Lastmatched] ,[lastvolume] ,[TotalVolume],[Turnover],[Open],[High],[Low],[Average Price] as AvgPrice,[Change],[percchange],[url],[url2]FROM[testcds_ROUTER].[dbo].[MarketWatch] where RTRIM(LTRIM(fnam))='" + name.Trim() + "'";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatch recordSummary = new MarketWatch();
                    recordSummary.Company = rdr["Company"].ToString();
                    recordSummary.Volume = rdr["Volume"].ToString();
                    recordSummary.Bid = rdr["Bid"].ToString();
                    recordSummary.VolumeSell = rdr["VolumeSell"].ToString();
                    recordSummary.Ask = rdr["Ask"].ToString();
                    recordSummary.LastTradedPrice = rdr["LastTradedPrice"].ToString();
                    recordSummary.Lastmatched = rdr["Lastmatched"].ToString();
                    recordSummary.Lastvolume = rdr["Lastvolume"].ToString();
                    recordSummary.TotalVolume = rdr["TotalVolume"].ToString();
                    recordSummary.Turnover = rdr["Turnover"].ToString();
                    recordSummary.Open = rdr["Open"].ToString();
                    recordSummary.High = rdr["High"].ToString();
                    recordSummary.Low = rdr["Low"].ToString();
                    recordSummary.AveragePrice = rdr["AvgPrice"].ToString();
                    recordSummary.Change = rdr["Change"].ToString();
                    recordSummary.Percchange = rdr["Percchange"].ToString();
                    marketWatcher.Add(recordSummary);
                }
            }

            if (marketWatcher.Any())
            {
                return Json(marketWatcher, JsonRequestBehavior.AllowGet);
            }
            return Json("No data was found", JsonRequestBehavior.AllowGet);
            //            var thePrices = "atsDbContext.;";
            //            return Json(thePrices, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResetPassword(string email, string natId, string newpass)
        {
            //            var cdDataContext = new CdDataContext();
            if (email != null || newpass != null || natId != null)
            {
                var usernameFound = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Email == email);
                if (usernameFound != null)
                {

                    var IdNumberFound = _cdDataContext.Accounts_Clients.FirstOrDefault(x => x.Email == email && x.IDNoPP == natId);

                    if (IdNumberFound != null)
                    {
                        usernameFound.Password = newpass;
                        try
                        {
                            _cdscDbContext.SaveChanges();
                            return Json(1, JsonRequestBehavior.AllowGet);
                        }
                        catch (Exception)
                        {
                            return Json(3, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(4, JsonRequestBehavior.AllowGet);

                }
                return Json(2, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ForgotPassword(string email, string natId, string newpass, string dob)
        {
            //            var cdDataContext = new CdDataContext();
            if (email != null || newpass != null || natId != null)
            {
                var usernameFound = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Email == email);
                if (usernameFound != null)
                {
                    DateTime ofBirth;
                    if (DateTime.TryParse(dob, out ofBirth))
                    {
                        var IdNumberFound = _cdDataContext.Accounts_Clients.FirstOrDefault(x => x.IDNoPP == natId && x.DOB == DateTime.Parse(dob));

                        if (IdNumberFound != null)
                        {
                            usernameFound.Password = newpass;
                            try
                            {
                                _cdscDbContext.SaveChanges();
                                return Json(1, JsonRequestBehavior.AllowGet);
                            }
                            catch (Exception)
                            {
                                return Json(3, JsonRequestBehavior.AllowGet);
                            }
                        }
                        return Json(4, JsonRequestBehavior.AllowGet);
                    }
                    return Json(5, JsonRequestBehavior.AllowGet);//failed to convert striing provided under dob into datetime

                }
                return Json(2, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        /*public JsonResult ResetPassword(string email, string natId, string newpass)
       {
           //            var cdDataContext = new CdDataContext();
           if (email != null || newpass != null || natId != null)
           {
               var usernameFound = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Email == email);
               if (usernameFound != null)
               {

                   //                    var userCtrade = cdDataContext.Accounts_Clients.FirstOrDefault(x => x.Email == email);
                   //
                   //                    if (userCtrade != null)
                   //                    {
                   //
                   //                    }

                   usernameFound.Password = newpass;
                   try
                   {
                       _cdscDbContext.SaveChanges();
                       return Json(1, JsonRequestBehavior.AllowGet);
                   }
                   catch (Exception)
                   {
                       return Json(3, JsonRequestBehavior.AllowGet);
                   }
               }
               return Json(2, JsonRequestBehavior.AllowGet);
           }
           return Json(0, JsonRequestBehavior.AllowGet);
       }*/


        public JsonResult ChangePassword(string oldpass, string newpass, string id)
        {
            if (oldpass != null || newpass != null || id != null)
            {
                //                var ids = int.Parse(id);

                var usernameFound = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Password == oldpass && x.Active && x.Email == id);
                if (usernameFound != null)
                {
                    usernameFound.Password = newpass;
                    try
                    {
                        _cdscDbContext.SaveChanges();
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        return Json(3, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(2, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeactivateAccount(string username, string password)
        {
            if (username != null || password != null)
            {
                var usernameFound = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Username == username && x.Password == password && x.Active);
                if (usernameFound != null)
                {
                    usernameFound.Active = false;
                    try
                    {
                        _cdscDbContext.SaveChanges();
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        return Json(3, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(2, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Subscribe(string id, string toAdd, string toRemove)
        {
            var itemsToRemove = new ArrayList();
            var itemsToAdd = new ArrayList();
            var count = 0;

            var addReturn = 0;
            var remReturn = 0;
            var ret = "0";

            if (toAdd != null)
            {
                itemsToAdd.AddRange(toAdd.Split('*'));
                itemsToAdd.RemoveAt(itemsToAdd.Count - 1);

                foreach (var adds in itemsToAdd)
                {
                    var theString = adds;
                    var theProduct =
                    _cdscDbContext.Products.OrderByDescending(x => x.Id)
                        .FirstOrDefault(x => x.Name == theString.ToString());

                    if (theProduct != null)
                    {
                        var theProductId = theProduct.Id;

                        //INSERTING INTO SUBSCRIBERPRODUCTS
                        var subscriberProduct = new SubscriberProduct
                        {
                            SubscriberId = int.Parse(id),
                            ProductId = theProductId,
                            Date = DateTime.Now
                        };

                        try
                        {
                            _cdscDbContext.SubscriberProducts.Add(subscriberProduct);
                            _cdscDbContext.SaveChanges();
                            addReturn++;
                        }
                        catch (Exception exception)
                        {
                            ret = exception.ToString();
                            addReturn = 0;
                        }
                    }
                }
            }


            if (toRemove != null)
            {
                itemsToRemove.AddRange(toRemove.Split('*'));
                itemsToRemove.RemoveAt(itemsToRemove.Count - 1);

                //                for (int i = 0; i < toRemove.Length; i++)
                foreach (var rev in itemsToRemove)
                {
                    var theString = rev;
                    var theProduct =
                        _cdscDbContext.Products.OrderByDescending(x => x.Id)
                            .FirstOrDefault(x => x.Name == theString.ToString());

                    if (theProduct != null)
                    {
                        var theProductId = theProduct.Id;
                        var code = _cdscDbContext.SubscriberProducts.FirstOrDefault(x => x.ProductId == theProductId);

                        if (code != null)
                        {
                            try
                            {
                                _cdscDbContext.SubscriberProducts.Remove(code);
                                _cdscDbContext.SaveChanges();
                                count++;
                                remReturn++;
                            }
                            catch (Exception exception)
                            {
                                ret = exception.ToString();
                            }
                        }
                    }
                }
            }
            //0. For any errors
            //1. For Add only
            //2. For Rev Only
            //3. For Add and Rev

            var rets = 0;

            if (toAdd != null && toAdd.Length > 0)
            {
                if (addReturn == itemsToAdd.Count)
                {
                    rets = 1;
                }
                else
                {
                    rets = 0;
                }
            }

            if (toRemove != null && toRemove.Length > 0)
            {
                if (count == itemsToRemove.Count)
                {
                    rets = 2;
                }
                else
                {
                    rets = 0;
                }
            }

            if (toRemove != null && toAdd != null)
            {
                if (addReturn == itemsToAdd.Count && count == itemsToRemove.Count)
                {
                    rets = 3;
                }
            }
            return Json(rets, JsonRequestBehavior.AllowGet);
        }


        public JsonResult AccountCreation(string branch, string mobile, string adress_yangu, string theTitle, string theSurname, string theName, string thenationality, string theDOB, string theGender, string theCountry, string idNums, string bank, string accountNum, string email, string password)
        {

            //        public JsonResult AccountCreation(string theTitle)
            //        {


            var rndNum = new Random();
            var rnNum = rndNum.Next(100000, 999999);
            var r = LongRandom(0, 999999999999999, new Random());

            //            Tel = " ",
            //                Mobile = " ",
            //            var accountsAudit = new Accounts_Clients()
            //            {
            //                Title = theTitle,
            //                CDS_Number = r.ToString(),
            //                AccountType = "I",
            //                BrokerCode = "CORP",
            //                Surname = theSurname,
            //                Forenames = theName,
            //                IDNoPP = idNums,
            //                DOB = DateTime.Parse(theDOB),
            //                Gender = theGender,
            //                Nationality = thenationality,
            //                Country = theCountry,
            //                Email = email,
            //                Div_Bank = bank,
            //                Div_AccountNo = accountNum
            //            };
            //
            //            try
            //            {
            //                cdDataContexts.Accounts_Clients.Add(accountsAudit);
            //
            //
            //                cdDataContexts.SaveChanges();
            //
            //                return Json(1, JsonRequestBehavior.AllowGet);

            //                TradingStatus = "DEALING ALLOWED",
            //                Category = "C",
            //                var subscriber = new Subscriber
            //                {
            //                    Email = email,
            //                    Username = email,
            //                    Password = password,
            //                    Active = true,
            //                    Date = DateTime.Now
            //                };
            //                try
            //                {
            //       
            //                    _cdscDbContext.Subscribers.Add(subscriber);
            //                    _cdscDbContext.SaveChanges();
            //
            //                    SendMail2(email, "You have successfully created C-Trade Account. Kindly note that this is not a trading account, its just an account to access market information. You will be requested to open a trading  account if you want to trade", "Access Account Opened");
            //                    return Json(1, JsonRequestBehavior.AllowGet);
            //                }
            //                catch (Exception exception)
            //                {
            //                    return Json(exception.ToString(), JsonRequestBehavior.AllowGet);
            //                }
            //try
            //{
            //                return ;

            Accounts_Clients_Web accountsClients = new Accounts_Clients_Web
            {
                Title = theTitle,
                CDS_Number = r.ToString(),
                AccountType = "I",
                BrokerCode = "CORP",
                Surname = theSurname,
                Forenames = theName,
                IDNoPP = idNums,
                DOB = DateTime.Parse(theDOB),
                Gender = theGender,
                Nationality = thenationality,
                Country = theCountry,
                Email = email,
                Div_Bank = bank,
                Div_AccountNo = accountNum,
                Middlename = "",
                Initials = "",
                IDtype = "",
                Add_1 = adress_yangu,
                Add_2 = "",
                Add_3 = "",
                Add_4 = "",
                City = "",
                Tel = mobile,
                Mobile = mobile,
                Category = "",
                Custodian = "",
                TradingStatus = "DEALING ALLOWED",
                Industry = "",
                Tax = "",
                Div_Branch = branch,
                Cash_Bank = "",
                Cash_Branch = "",
                Cash_AccountNo = "",
                Update_Type = "",
                Created_By = "",
                AccountState = "",
                Comments = "",
                DivPayee = "",
                SettlementPayee = "",
                Account_class = "",
                idnopp2 = "",
                idtype2 = "",
                client_image2 = "",
                documents2 = "",
                isin = "",
                sttupdate = true,
                company_code = "",
                mobile_money = "",
                mobile_number = "",
                Attached_Documents = "",
                Date_Created = DateTime.Now
            };



            _cdDataContext.Accounts_Clients_Web.Add(accountsClients);
            _cdDataContext.SaveChanges();

            var subscriber = new Subscriber
            {
                Email = email,
                Username = email,
                CdsNumber = GetCdsNumberFROMIDNUM(idNums),
                Password = password,
                Active = true,
                Date = DateTime.Now
            };
            sendmail(email, "You have successfully created C-Trade Account. CDS Number " +
                GetCdsNumberFROMIDNUM(idNums), "Access Account Opened");
            _cdscDbContext.Subscribers.Add(subscriber);
            _cdscDbContext.SaveChanges();

            return Json(1, JsonRequestBehavior.AllowGet);
            //}
            //catch (Exception ex)
            //{
            //    return Json(ex, JsonRequestBehavior.AllowGet);
            //}
        }

        public JsonResult AccountMaintenance(string cdsNum, string theAddress, string theCountry, string theTelephone, string theMobile, string theEmail, string theAccName, string theAccNum, string theBank, string theBranch,
                           string theMobileMoney, string theMobileMoneyNumber)
        {
            var found = _cdDataContext.Account_Creation.FirstOrDefault(x => x.CDSC_Number == cdsNum);

            if (found != null)
            {
                found.Address1 = theAddress;
                found.Country = theCountry;
                found.TelephoneNumber = theTelephone;
                found.Emailaddress = theEmail;
                found.Accountnumber = theAccNum;
                found.Bank = theBank;
                found.Branch = theBranch;

                try
                {
                    _cdDataContext.SaveChanges();
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckWhoRegistered(string any)
        {
            var rets = _cdDataContext.Accounts_Clients.FirstOrDefault(x => x.Email.Trim() == any || x.CDS_Number == any.Trim());
            if (rets != null)
            {
                var mobAccount = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Email == any.Trim() || x.CdsNumber == any.Trim());
                if (mobAccount != null)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                return Json(rets, JsonRequestBehavior.AllowGet);
            }
            //create new account
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckWhoRegisteredCtrade(string any)
        {
            var rets = _cdDataContext.Accounts_Clients.FirstOrDefault(x => x.Email.Trim() == any || x.CDS_Number == any.Trim());

            var mobAccount = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Email == any.Trim() || x.CdsNumber == any.Trim());
            if (mobAccount != null)
            {
                if (rets != null)
                {
                    return Json(1, JsonRequestBehavior.AllowGet);//its in cds and not in ctrade
                }
                else
                {
                    return Json(3, JsonRequestBehavior.AllowGet);//its in  ctrade only
                }
            }
            else if (rets != null)
            {
                return Json(2, JsonRequestBehavior.AllowGet);//its in cds and not in ctrade
            }
            else
            {
                //create new account
                return Json(0, JsonRequestBehavior.AllowGet);//its not in either so create
            }

        }


        public JsonResult InvestorReg(string idNumber, string pass)
        {
            //            return 
            //            return Json(idNumber + "  " + pass, JsonRequestBehavior.AllowGet);
            //            x => x.Username.Trim() == "rfde@gmail.com" && x.Password.Trim() == "12341234"


            var cdDataContexts = new cdscDbContext();
            var usernameFound = cdDataContexts.Subscribers.FirstOrDefault(x => (x.Username.Trim() == idNumber.Trim() || x.CdsNumber.Trim() == idNumber.Trim()) && x.Password == pass.Trim());


            if (usernameFound != null)
            {
                if (usernameFound.Active == false)
                {
                    return Json(8, JsonRequestBehavior.AllowGet);
                }
                //                return Json("2", JsonRequestBehavior.AllowGet);
                var user = _cdDataContext.Accounts_Clients.FirstOrDefault(x => (x.Email.Trim() == idNumber.Trim() || x.CDS_Number.Trim() == idNumber.Trim()));
                if (user != null)
                {
                    Random generator = new Random();
                    string GenerateRandomNo = generator.Next(1, 10000).ToString("D4");
                    var rets =
                        _cdDataContext.Accounts_Clients.Where(x => (x.Email.Trim() == idNumber.Trim() || x.CDS_Number.Trim() == idNumber.Trim()))
                            .OrderByDescending(x => x.ID)
                            .Select(c => new
                            {
                                id = c.CDS_Number,
                                brokerName = c.Custodian,
                                broker = c.BrokerCode,
                                cds = c.CDS_Number,
                                email = c.Email,
                                name = c.Surname + " " + c.Forenames,
                                phone = c.Mobile,
                                pin = GenerateRandomNo
                            });

                    String sqlx = "";
                    foreach (var p in rets)
                    {
                        var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
                        sqlx = "Update Subscribers set PIN ='" + GenerateRandomNo + "' where CdsNumber='" + p.cds.ToString() + "' ";
                        //Console.WriteLine("Hello World!" + sqlx);
                        //return Json(sqlx, JsonRequestBehavior.AllowGet);
                        //messagesend_MOBI(p.phone, "Your pin is " + GenerateRandomNo); 
                        using (SqlConnection connectionsx = new SqlConnection(connectionString))
                        {
                            SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                            connectionsx.Open();
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();

                        }
                        //connectionsx.Close();
                        break;
                    }

                    /*if(rets != null)
                    {
                    string subject = "CTradeMobile login pin";
                    string emailBody = "Your one time pin is " + GenerateRandomNo;
                    sendmail(user.Email, subject, emailBody);
                    }*/

                    return Json(rets, JsonRequestBehavior.AllowGet);
                }
                return Json("No email in accounts creation", JsonRequestBehavior.AllowGet);
            }
            //1. Checking from Accounts_Creation
            return Json("No email in registrations", JsonRequestBehavior.AllowGet);
        }

        public JsonResult InvestorRegTest(string idNumber, string pass)
        {
            //            return 
            //            return Json(idNumber + "  " + pass, JsonRequestBehavior.AllowGet);
            //            x => x.Username.Trim() == "rfde@gmail.com" && x.Password.Trim() == "12341234"


            var cdDataContexts = new cdscDbContext();
            var usernameFound = cdDataContexts.Subscribers.FirstOrDefault(x => (x.Username.Trim() == idNumber.Trim() || x.CdsNumber.Trim() == idNumber.Trim()) && x.Password == pass.Trim());

            if (usernameFound != null)
            {
                if (usernameFound.Active == false)
                {
                    return Json(8, JsonRequestBehavior.AllowGet);
                }
                //                return Json("2", JsonRequestBehavior.AllowGet);
                var user = _cdDataContext.Accounts_Clients.FirstOrDefault(x => (x.Email.Trim() == idNumber.Trim() || x.CDS_Number.Trim() == idNumber.Trim()));
                if (user != null)
                {
                    Random generator = new Random();
                    string GenerateRandomNo = generator.Next(1, 10000).ToString("D4");
                    var rets =
                        _cdDataContext.Accounts_Clients.Where(x => (x.Email.Trim() == idNumber.Trim() || x.CDS_Number.Trim() == idNumber.Trim()))
                            .OrderByDescending(x => x.ID)
                            .Select(c => new
                            {
                                id = c.CDS_Number,
                                brokerName = c.Custodian,
                                broker = c.BrokerCode,
                                cds = c.CDS_Number,
                                email = c.Email,
                                name = c.Surname + " " + c.Forenames,
                                phone = c.Mobile,
                                pin = GenerateRandomNo
                            });

                    String sqlx = "";
                    foreach (var p in rets)
                    {

                        var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
                        sqlx = "Update Subscribers set PIN ='" + GenerateRandomNo + "' where CdsNumber='" + p.cds.ToString() + "' ";
                        using (SqlConnection connectionsx = new SqlConnection(connectionString))
                        {
                            SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                            connectionsx.Open();
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();

                        }
                        //connectionsx.Close();
                        break;
                    }
                    try
                    {
                        SmtpClient client = new SmtpClient();
                        client.Port = 587;
                        client.Host = "smtp.gmail.com";
                        client.EnableSsl = true;
                        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                        client.Timeout = 30000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        //client.Credentials = new System.Net.NetworkCredential("mobilectrade@gmail.com", "asdfghjkl2017");
                        client.Credentials = new System.Net.NetworkCredential("no-reply@ctrade.co.zw", "asdfghjkl2018");
                        MailMessage mm = new MailMessage();
                        mm.BodyEncoding = Encoding.UTF8;
                        mm.From = new MailAddress("no-reply@ctrade.co.zw");
                        mm.To.Add(user.Email);
                        mm.Subject = "C-TRADE Mobile OTP";
                        mm.Body = "Your OTP is " + GenerateRandomNo;
                        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        client.Send(mm);
                        return Json(rets, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        return Json("Error sending OTP Please try again", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("No email in accounts creation", JsonRequestBehavior.AllowGet);
            }
            //1. Checking from Accounts_Creation
            return Json("No email in registrations", JsonRequestBehavior.AllowGet);
        }

        public JsonResult InvestorRegAuto(string idNumber, string pass)
        {
            //            return 
            //            return Json(idNumber + "  " + pass, JsonRequestBehavior.AllowGet);
            //            x => x.Username.Trim() == "rfde@gmail.com" && x.Password.Trim() == "12341234"


            var cdDataContexts = new cdscDbContext();
            var usernameFound = cdDataContexts.Subscribers.FirstOrDefault(x => (x.Username.Trim() == idNumber.Trim() || x.CdsNumber.Trim() == idNumber.Trim()) && x.Password == pass.Trim());

            if (usernameFound != null)
            {
                if (usernameFound.Active == false)
                {
                    return Json(8, JsonRequestBehavior.AllowGet);
                }
                //                return Json("2", JsonRequestBehavior.AllowGet);
                var user = _cdDataContext.Accounts_Clients.FirstOrDefault(x => (x.Email.Trim() == idNumber.Trim() || x.CDS_Number.Trim() == idNumber.Trim()));
                if (user != null)
                {
                    Random generator = new Random();
                    string GenerateRandomNo = generator.Next(1, 10000).ToString("D4");
                    var rets =
                        _cdDataContext.Accounts_Clients.Where(x => (x.Email.Trim() == idNumber.Trim() || x.CDS_Number.Trim() == idNumber.Trim()))
                            .OrderByDescending(x => x.ID)
                            .Select(c => new
                            {
                                id = c.CDS_Number,
                                brokerName = c.Custodian,
                                broker = c.BrokerCode,
                                cds = c.CDS_Number,
                                email = c.Email,
                                name = c.Surname + " " + c.Forenames,
                                phone = c.Mobile,
                                pin = GenerateRandomNo
                            });

                    String sqlx = "";
                    foreach (var p in rets)
                    {

                        var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
                        sqlx = "Update Subscribers set PIN ='" + GenerateRandomNo + "' where CdsNumber='" + p.cds.ToString() + "' ";
                        using (SqlConnection connectionsx = new SqlConnection(connectionString))
                        {
                            SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                            connectionsx.Open();
                            cmd.CommandType = CommandType.Text;
                            cmd.ExecuteNonQuery();

                        }
                        //connectionsx.Close();
                        break;
                    }
                    try
                    {
                        SmtpClient client = new SmtpClient();
                        client.Port = 587;
                        client.Host = "smtp.gmail.com";
                        client.EnableSsl = true;
                        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                        client.Timeout = 30000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential("escrowctrade@gmail.com", "asdfghjkl2017");
                        MailMessage mm = new MailMessage();
                        mm.BodyEncoding = Encoding.UTF8;
                        mm.From = new MailAddress("no-reply@ctrade.co.zw");
                        mm.To.Add(user.Email);
                        mm.Subject = "C-TRADE Mobile login pin";
                        mm.Body = "Your OTP is " + GenerateRandomNo;
                        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        client.Send(mm);
                        return Json(rets, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        return Json("Error sending OTP Please try again", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("No email in accounts creation", JsonRequestBehavior.AllowGet);
            }
            //1. Checking from Accounts_Creation
            return Json("No email in registrations", JsonRequestBehavior.AllowGet);
        }

        string fileName = "C:\\uss\\Loggingdatarangu.txt";
        private void writetofile(String mssg)
        {

            StreamWriter objWriter3 = new System.IO.StreamWriter(fileName, true);
            objWriter3.WriteLine("Any Message ipapapa" + mssg);
            objWriter3.Close();
        }

        public string messagesend_MOBI(string mobile, string message)
        {
            String my_rteturn = "";
            try
            {
                string mob_number = mobile.Replace(" ", "");
                if (mob_number.StartsWith("0") == true)
                {
                    mob_number = Regex.Replace(mob_number, "^0", "263");
                }
                else if (mob_number.StartsWith("263") == false)
                {
                    mob_number = "263" + mob_number;
                }
                mob_number = "263772166492";
                System.Net.WebClient client = new System.Net.WebClient();
                String url_req = "http://etext.co.zw/sendsms.php?user=263773360785&password=simbaj80&mobile=" + mob_number.ToString() + "&senderid=C-TRADE&message=" + message + "";
                my_rteturn = client.DownloadString(url_req);
                writetofile("Phone " + mob_number + " " + my_rteturn + " " + url_req);
            }
            catch (Exception ex)
            {
                writetofile(ex.ToString());
                my_rteturn = "";
            }
            return my_rteturn;
        }

        public JsonResult Subscriptions(int id)
        {
            var rets = _cdscDbContext.Products.Where(x => x.Active).Select(c => new
            {
                id = c.Id,
                name = c.Name
            });
            return Json(rets, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MySubscriptions(int id)
        {

            var ret = _cdscDbContext.SubscriberProducts.Join(_cdscDbContext.Products, c => c.ProductId, cd => cd.Id,
                (c, cd) => new
                {
                    Name = cd.Name,
                    Id = c.Id,
                    ProductId = cd.Id,
                    SubscriberId = c.SubscriberId
                }).Where(cd => cd.SubscriberId == id);

            return Json(ret, JsonRequestBehavior.AllowGet);
        }

        public string UpdateBroker(string broker, string cdsnumber)
        {
            string returnstring = "";
            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            string sqlx = "Update Accounts_Clients set BrokerCode ='" + broker + "' where CDS_Number='" + cdsnumber + "' ";

            using (SqlConnection connectionsx = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                connectionsx.Open();
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                returnstring = "Successfully Updated";

            }
            return returnstring;
        }


        public string UpdateCancelOrder(string orderType, string cdsnumber, string ordernumber)
        {
            string returnstring = "";
            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            string sqlx = "Update Pre_Order_Live set OrderStatus ='CANCELLED' where OrderNo ='" + ordernumber
                + "' and CDS_AC_No='" + cdsnumber + "' ";

            using (SqlConnection connectionsx = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sqlx, connectionsx);
                connectionsx.Open();
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                returnstring = "Successfully Cancelled";

            }
            return returnstring;
        }



        public JsonResult NewLog(string idNumber = null)
        {
            var getBrk = "select ac.CDS_Number as id, cc.Company_name as brokerCode, ac.CDS_Number as CdsNumber from [CDS_ROUTER].[dbo].Accounts_Clients ac join [CDS_ROUTER].[dbo].Client_Companies cc on ac.BrokerCode = cc.Company_Code where ac.IDNoPP='" + idNumber + "'";

            var newLogs = new List<NewLog>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    NewLog newLog = new NewLog();
                    newLog.Id = rdr["id"].ToString();
                    newLog.BrokerCode = rdr["brokerCode"].ToString();
                    newLog.CdsNumber = rdr["CdsNumber"].ToString();
                    newLogs.Add(newLog);
                }

                try
                {
                    var jsonResult = Json(newLogs, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception)
                {
                    return Json("Data set too large", JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult Login(string username = null, string password = null)
        {
            if (username != null || password != null)
            {
                var usernameFound = _cdscDbContext.Subscribers.FirstOrDefault(x => (x.Username.Trim() == username.Trim() || x.CdsNumber.Trim() == username.Trim()) && (x.Password.Trim() == password.Trim() && x.Active));
                if (usernameFound != null)
                {
                    var ret = _cdscDbContext.Subscribers.Where(x => (x.Username.Trim() == username.Trim() || x.CdsNumber.Trim() == username.Trim()) && x.Password.Trim() == password.Trim()).OrderByDescending(x => x.Id).Select(v => new
                    {
                        id = v.Id,
                        username = v.Username,
                        email = v.Email,
                        cds = v.CdsNumber,

                        //fullname = getAccountFullname(cds)
                    }).Take(1);

                    return Json(ret, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
        }

        public JsonResult NewLogin(string username = null, string password = null)
        {

            if (username != null || password != null)
            {

                var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
                var CompShares = new List<SubscriberNew>();

                var sql = "SELECT t.* , ut.BrokerCode  FROM [CDSC].[dbo].[Subscribers] t , [CDS_ROUTER].[dbo].[Accounts_Clients] ut where t.Email ='" + username + "' and t.Password = '" + password + "' and t.CdsNumber = ut.CDS_Number";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(sql, connection);
                    cmd.CommandType = CommandType.Text;
                    connection.Open();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        SubscriberNew recordSummary = new SubscriberNew();
                        recordSummary.id = int.Parse(rdr["id"].ToString());
                        recordSummary.username = rdr["username"].ToString();
                        recordSummary.email = rdr["email"].ToString();
                        recordSummary.broker = rdr["BrokerCode"].ToString();
                        recordSummary.cds = rdr["CdsNumber"].ToString();
                        recordSummary.Active = Convert.ToBoolean(rdr["Active"].ToString());
                        CompShares.Add(recordSummary);
                    }
                }

                if (CompShares.Any())
                {
                    return Json(CompShares, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(0, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(0, JsonRequestBehavior.AllowGet);

        }


        // GET: Subscriber
        public JsonResult Registration(string email = null, string username = null, string password = null)
        {
            var usernameFound = _cdscDbContext.Subscribers.FirstOrDefault(x => x.Username == username && x.Active && x.Email == email);
            if (usernameFound == null)
            {
                if (email != null || username != null || password != null)
                {
                    var subscriber = new Subscriber
                    {
                        Email = email,
                        Username = username,
                        CdsNumber = username,
                        Password = password,
                        Active = true,
                        Date = DateTime.Now
                    };
                    try
                    {
                        _cdscDbContext.Subscribers.Add(subscriber);
                        _cdscDbContext.SaveChanges();

                        sendmail(email, "You have successfully created C-Trade Account. Kindly note that this is not a trading account, its just an account to access market information. You will be requested to open a trading  account if you want to trade", "Access Account Opened");
                        return Json(1, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception exception)
                    {
                        return Json(exception.ToString(), JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            return Json(2, JsonRequestBehavior.AllowGet);
        }
        public string SendMailAgain(string Message, string Subject)
        {
            try
            {
                bool retVal;
                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = new MailMessage();

                MailAddress fromAddres = new MailAddress("info@ctrade.co.zw", "Test");
                message.From = fromAddres;
                // To address collection of MailAddress
                message.To.Add("makazatinashe2000@gmail.com");
                message.Subject = Subject;
                smtpClient.Host = "192.168.3.241";
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential("info@ctrade.co.zw", "asdfghjkl2017");
                message.IsBodyHtml = true;
                // Message body content
                message.Body = Message;

                smtpClient.Send(message);

                retVal = true;
                message.Dispose();
                return "Message Sent";
            }
            catch (Exception ex)
            {
                return "Error " + ex;
            }

        }
        /*public string sendmail(string emailAdd, string subject, string emailbody)
        {
            try
            {

                MailMessage Mail = new MailMessage();
                Mail.Subject = subject;
                Mail.To.Add("" + emailAdd + "");
                // Mail.From = New MailAddress("corpservesharepower@googlemail.com")
                Mail.From = new MailAddress("notifications@ctrade.co.zw");
                Mail.Body = emailbody;
                // Dim SMTP As New SmtpClient("smtp.googlemail.com")
                System.Net.ServicePointManager.ServerCertificateValidationCallback = (object se, System.Security.Cryptography.X509Certificates.X509Certificate cert, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslerror) => true;
                //Dim SMTP As New SmtpClient("64.233.167.16")
                SmtpClient SMTP = new SmtpClient("192.168.3.241");
                SMTP.EnableSsl = true;
                SMTP.Credentials = new System.Net.NetworkCredential("notifications@ctrade.co.zw", "asdfghjkl2017");
                SMTP.Port = 587;
                // SMTP.Port = SMTPport
                SMTP.Send(Mail);
            
                return "Email Send";
            }
            catch (Exception ex)
            {
                return "Error sending email " + ex;
            }
        }*/

        public string sendmail(string emailAdd, string subject, string emailbody)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                //client.Host = "smtp.office365.com";
                client.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                client.Timeout = 30000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("escrowctrade@gmail.com", "asdfghjkl2017");
                //client.Credentials = new System.Net.NetworkCredential("info@ctrade.co.zw", "asdfghjkl.2017");
                MailMessage mm = new MailMessage();
                mm.BodyEncoding = Encoding.UTF8;
                mm.From = new MailAddress("info@ctrade.co.zw");
                mm.To.Add(emailAdd);
                mm.Subject = subject;
                mm.Body = emailbody;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);

                return "1";
            }
            catch (Exception e)
            {
                return "0";
            }
        }


        public void SendMail2(string emailAddess, string message, string subject)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("makazatinashe2000@gmail.com", "Lockthemout@2017");

                MailMessage mm = new MailMessage();
                mm.BodyEncoding = Encoding.UTF8;
                mm.From = new MailAddress("makazatinashe2000@gmail.com");
                mm.To.Add(emailAddess);
                mm.Subject = subject;
                mm.Body = message;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);
                //                return "1";
            }
            catch (Exception e)
            {
                //                return e.ToString();
            }
        }
        public JsonResult CashTrans(string cdsNumber = null)
        {
            var ret = _cdscDbContext.CashTrans.Where(x => x.CDS_Number.Trim() == cdsNumber.Trim()).OrderByDescending(x => x.ID).Select(v => new
            {
                id = v.ID,
                desc = v.Description,
                type = v.TransType,
                ammount = v.Amount,
                date = v.DateCreated,
                status = v.TransStatus
            });
            List<CashTranss> my = new List<CashTranss>();
            foreach (var p in ret)
            {
                my.Add(new CashTranss() { id = p.id.ToString(), desc = p.desc, type = p.type, ammount = p.ammount, date = p.date.ToString("dd MMM yyyy"), status = p.status });
            }

            return Json(my, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCashBalance(string cdsNumber = null)
        {
            var CashBalancc = new List<CashBalanc>();
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var ComptotalAccount = 0.00;

            var sql = @"
                  SELECT
                  isnull(SUM(TotPottValue), 0) as MyPotValue,
                  ISNULL(SUM([totReturnValue]),0) AS MyProfitLoss,
                  (select isnull(sum(Amount), 0) from CashTrans where CDS_Number = '" + cdsNumber + @"' ) as CashBal , 
                   (select isnull(sum(Amount), 0) from CashTrans where CDS_Number = '" + cdsNumber + @"' and [TransStatus] = '2' ) as VirtCashBal
                  FROM[CDSC].[dbo].[MyportfolioAll] WHERE CDS_NUMBER = '" + cdsNumber + @"'
                ";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                var ix = 1;
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    CashBalanc recordSummary = new CashBalanc();
                    recordSummary.id = ix.ToString();
                    recordSummary.CashBal = Force2DecimalPlaces(rdr["CashBal"].ToString());
                    recordSummary.VirtCashBal = Force2DecimalPlaces(rdr["VirtCashBal"].ToString());
                    recordSummary.MyPotValue = Force4DecimalPlaces(rdr["MyPotValue"].ToString());
                    recordSummary.MyProfitLoss = Force4DecimalPlaces(rdr["MyProfitLoss"].ToString());
                    ComptotalAccount = double.Parse(rdr["CashBal"].ToString()) + double.Parse(rdr["MyPotValue"].ToString());
                    recordSummary.totalAccount = Force4DecimalPlaces(ComptotalAccount.ToString());
                    CashBalancc.Add(recordSummary);
                }
            }
            return Json(CashBalancc, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MarketWatchbidoffer()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["AtsDbContext"].ConnectionString;
            var marketWatcherNeww = new List<MarketWatchNeww>();

            //var sql = "SELECT * FROM [testcds_ROUTER].[dbo].[MarketWatch]";
            var sql = "SELECT a.*, b.fnam as full_company_name FROM [testcds_ROUTER].[dbo].[MarketWatch] a, testcds_ROUTER.dbo.para_company b WHERE a.company = b.Company";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MarketWatchNeww recordSummary = new MarketWatchNeww();
                    recordSummary.market_company = rdr["company"].ToString();
                    recordSummary.market_bbv = rdr["Volume"].ToString();
                    recordSummary.market_bp = Force4DecimalPlaces(rdr["Bid"].ToString());
                    recordSummary.market_va = rdr["Volume Sell"].ToString();
                    recordSummary.market_ap = Force4DecimalPlaces(rdr["Ask"].ToString());
                    recordSummary.market_vwap = Force4DecimalPlaces(rdr["Average Price"].ToString());
                    recordSummary.market_lp = Force4DecimalPlaces(rdr["Lastmatched"].ToString());
                    recordSummary.market_lv = rdr["lastvolume"].ToString();
                    recordSummary.market_tv = rdr["TotalVolume"].ToString();
                    recordSummary.market_to = Force4DecimalPlaces(rdr["Turnover"].ToString());
                    recordSummary.market_open = Force4DecimalPlaces(rdr["Open"].ToString());
                    recordSummary.market_high = Force4DecimalPlaces(rdr["High"].ToString());
                    recordSummary.market_low = Force4DecimalPlaces(rdr["Low"].ToString());
                    recordSummary.market_change = Force4DecimalPlaces(rdr["Change"].ToString());
                    recordSummary.market_per_change = Force4DecimalPlaces(Math.Round(double.Parse(rdr["percchange"].ToString()), 4).ToString());
                    recordSummary.details = rdr["company"].ToString();
                    recordSummary.FullCompanyName = rdr["full_company_name"].ToString();
                    //get bids
                    var retBids = _AtsDbContext.LiveTradingMasters.OrderByDescending(y => y.BasePrice).Where(x => x.OrderType == "BUY" && x.Company == recordSummary.market_company).Select(v => new
                    {
                        idd = v.OrderNo,
                        volumed = v.Quantity,
                        priced = v.BasePrice
                    });
                    var bidPriceser = new List<bidPrices_correct>();

                    foreach (var p in retBids)
                    {
                        bidPrices_correct recordSum1 = new bidPrices_correct();
                        recordSum1.id = p.idd.ToString();
                        recordSum1.price = Force4DecimalPlaces(p.priced.ToString());
                        recordSum1.volume = p.volumed.ToString();
                        bidPriceser.Add(recordSum1);
                        recordSummary.bids = bidPriceser;
                    }


                    //get bids

                    //get offers
                    var retOffers = _AtsDbContext.LiveTradingMasters.OrderBy(y => y.BasePrice).Where(x => x.OrderType == "SELL" && x.Company == recordSummary.market_company).Select(v => new
                    {
                        ido = v.OrderNo,
                        volumeo = v.Quantity,
                        priceo = v.BasePrice
                    });
                    var offerPriceser = new List<offerPrices>();

                    foreach (var p in retOffers)
                    {
                        offerPrices recordSum2 = new offerPrices();
                        recordSum2.id = p.ido.ToString();
                        recordSum2.price = Force4DecimalPlaces(p.priceo.ToString());
                        recordSum2.volume = p.volumeo.ToString();
                        offerPriceser.Add(recordSum2);
                        recordSummary.asks = offerPriceser;
                    }


                    //get offers

                    marketWatcherNeww.Add(recordSummary);
                }
            }
            return Json(marketWatcherNeww, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getIPOISSUES()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            var para_hold = new List<para_holding>();

            var sql = "SELECT * FROM para_holding WHERE IPOSTATUS = '0' ORDER BY ID_ DESC";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    para_holding recordSummary = new para_holding();

                    recordSummary.Category = rdr["Category"].ToString();
                    recordSummary.Issuer_Code = rdr["Issuer_Code"].ToString();
                    recordSummary.Debt_Type = rdr["Debt_Type"].ToString();
                    recordSummary.Security_Description = rdr["Security_Description"].ToString();
                    recordSummary.GlobalLimit = Decimal.Parse(rdr["GlobalLimit"].ToString());
                    recordSummary.IndividualLimit = Decimal.Parse(rdr["IndividualLimit"].ToString());
                    recordSummary.DailyLimit = Decimal.Parse(rdr["DailyLimit"].ToString());
                    recordSummary.BidRatio = Decimal.Parse(rdr["BidRatio"].ToString());
                    recordSummary.IPOSTATUS = int.Parse(rdr["IPOSTATUS"].ToString());
                    recordSummary.globLowerlimit = Decimal.Parse(rdr["globLowerlimit"].ToString());
                    recordSummary.InterestRate = Decimal.Parse(rdr["InterestRate"].ToString());
                    //recordSummary.IPOClosedDate = DateTime.Parse(rdr["IPOClosedDate"].ToString());
                    //recordSummary.Transaction_Limit = Decimal.Parse(rdr["Transaction_Limit"].ToString());
                    //recordSummary.firstlimit = Decimal.Parse(rdr["FirstLimit"].ToString());
                    //recordSummary.multiples = Decimal.Parse(rdr["Multiples"].ToString());
                    para_hold.Add(recordSummary);
                }
            }
            return Json(para_hold, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMyOrders(string cdsNumber = null)
        {
            // var retID = 0;
            var rets = _AtsDbContext.Pre_Order_Live.Where(x => x.CDS_AC_No == cdsNumber.Trim()).OrderByDescending(x => x.OrderNo).Select(c => new
            {
                //id = retID+1,
                id = c.OrderNo,
                counter = c.Company,
                type = c.OrderType,
                volume = c.Quantity.ToString(),
                price = c.BasePrice.ToString(),
                date_ = c.Create_date.ToString(),
                status = c.OrderStatus,
                desc = c.trading_platform + " - " + c.Broker_Code
            });
            List<Pre_Order_Lives> my = new List<Pre_Order_Lives>();
            foreach (var p in rets)
            {
                my.Add(new Pre_Order_Lives() { date = p.date_, id = p.id.ToString(), counter = p.counter.ToString(), type = p.type.ToString(), volume = p.volume.ToString(), price = p.price.ToString(), status = p.status.ToString(), desc = p.desc.ToString() });
            }
            return Json(my, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMyOrders1(string cdsNumber = null)
        {
            // var retID = 0;
            var order = from c in _AtsDbContext.Pre_Order_Live.Where(x => x.CDS_AC_No == cdsNumber.Trim()).OrderByDescending(x => x.OrderNo)
                        join t in _AtsDbContext.para_company on c.Company equals t.Company
                        let companyFullName = t.fnam
                        let id = c.OrderNo
                        let counter = c.Company
                        let type = c.OrderType
                        let volume = c.Quantity.ToString()
                        let price = c.BasePrice.ToString()
                        let date_ = c.Create_date.ToString()
                        let status = c.OrderStatus
                        let desc = c.trading_platform + " - " + c.Broker_Code
                        select new
                        {
                            id,
                            companyFullName,
                            counter,
                            type,
                            volume,
                            price,
                            date_,
                            status,
                            desc
                        };
            List<Pre_Order_Lives> my = new List<Pre_Order_Lives>();
            foreach (var p in order)
            {
                my.Add(new Pre_Order_Lives() { date = p.date_, id = p.id.ToString(), counter = p.counter.ToString(), type = p.type.ToString(), volume = p.volume.ToString(), price = p.price.ToString(), status = p.status.ToString(), desc = p.desc.ToString(), fullname = p.companyFullName.ToString() });
            }
            my = my.OrderByDescending(a => a.id).ToList();
            return Json(my, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetMyIPOOrders(string cdsNumber = null)
        {
            // var retID = 0;
            var rets = _cdscDbContext.PreOrderLivesIPOes.Where(x => x.CDS_AC_No == cdsNumber.Trim()).OrderByDescending(x => x.ID).Select(c => new
            {
                //id = retID+1,
                id = c.ID,
                counter = c.Company,
                type = c.OrderType,
                volume = c.Quantity.ToString(),
                price = c.BasePrice.ToString(),
                date = c.Create_date,
                status = c.OrderStatus,
                desc = c.Source + " - " + c.Broker_Code
            });
            List<PreOrderLivesIPOTest> my = new List<PreOrderLivesIPOTest>();
            foreach (var p in rets)
            {
                my.Add(new PreOrderLivesIPOTest() { id = p.id.ToString(), counter = p.counter.ToString(), type = p.type.ToString(), volume = p.volume.ToString(), price = p.price.ToString(), date = p.date.ToString("dd MMM yyyy"), status = p.status.ToString(), desc = p.desc.ToString() });
            }
            return Json(my, JsonRequestBehavior.AllowGet);
        }
        public decimal getTransPrice(decimal transID)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            decimal pricee = 0;
            var sql = "SELECT ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = a.Reference) AND (Account1 = a.CDS_Number)) OR ((ReportID = a.Reference) AND (Account2 = a.CDS_Number))),0) AS THEPRICE FROM CDS_ROUTER.DBO.trans A  WHERE A.Trans_ID='" + transID + "'";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    pricee = decimal.Parse(rdr["THEPRICE"].ToString());
                }
            }
            return pricee;
        }

        public JsonResult getMyPortFolioOriginal(string cdsNumber = null)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var myPortf = new List<MyPortfolio>();

            var sql = "select * from cdsc.dbo.MyportfolioAll d where d.CDS_Number='" + cdsNumber + "'";
            var ix = 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MyPortfolio recordSummary = new MyPortfolio();
                    recordSummary.id = ix.ToString();
                    recordSummary.counter = rdr["Company"].ToString();
                    recordSummary.numbershares = rdr["totAllShares"].ToString();
                    recordSummary.lastactivitydate = rdr["LastAcDate"].ToString();
                    recordSummary.currentprice = Force4DecimalPlaces(rdr["currePrice"].ToString());
                    recordSummary.prevprice = Force4DecimalPlaces(rdr["PrevPrice"].ToString());
                    recordSummary.totalportvalue = Force4DecimalPlaces(rdr["TotPottValue"].ToString());
                    recordSummary.totalPrevPortValue = Force4DecimalPlaces(Math.Round((double.Parse(rdr["PrevPrice"].ToString()) * double.Parse(rdr["totAllShares"].ToString())), 4).ToString());

                    recordSummary.returns = Force4DecimalPlaces(Math.Round(double.Parse(rdr["totReturnValue"].ToString()), 4).ToString());
                    //get my buys
                    ////var retBids = _cdDataContext.trans.Where(x => x.CDS_Number == cdsNumber.ToString() && x.Shares > 0 && x.Company == recordSummary.counter).Select(v => new
                    ////{
                    ////    comp = v.Company,
                    ////    volum = v.Shares,
                    ////    pric = v.Shares,
                    ////    totVal = v.Shares
                    ////});
                    var BuysBymes = new List<BuysByme>();
                    var sql2 = "select *,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares>0";
                    using (SqlConnection connection2 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd2 = new SqlCommand(sql2, connection2);
                        cmd2.CommandType = CommandType.Text;
                        connection2.Open();
                        SqlDataReader rdr2 = cmd2.ExecuteReader();
                        while (rdr2.Read())
                        {
                            BuysByme recordSum1 = new BuysByme();
                            recordSum1.company = rdr2["Company"].ToString();
                            recordSum1.volume = rdr2["Shares"].ToString();
                            recordSum1.price = Force4DecimalPlaces(rdr2["TradePrice"].ToString());
                            recordSum1.totalValue = Force4DecimalPlaces((decimal.Parse(rdr2["TradePrice"].ToString()) * decimal.Parse(rdr2["Shares"].ToString())).ToString());
                            BuysBymes.Add(recordSum1);
                        }
                    }
                    // foreach (var p in retBids)
                    //{

                    //}
                    recordSummary.BuyDetail = BuysBymes;
                    //get my buys

                    //get my sells
                    //var retOffers = _cdDataContext.trans.Where(x => x.CDS_Number == cdsNumber.ToString() && x.Shares < 0 && x.Company == recordSummary.counter).Select(v => new
                    //{
                    //    comp = v.Company,
                    //    volum = v.Shares,
                    //    pric = v.Shares,
                    //    totVal = v.Shares
                    //});
                    //var sellBymes = new List<SellsByme>();

                    //foreach (var p in retOffers)
                    //{
                    //    SellsByme recordSum2 = new SellsByme();
                    //    recordSum2.company = p.comp.ToString();
                    //    recordSum2.volume = p.volum.ToString();
                    //    recordSum2.price = p.pric.ToString();
                    //    recordSum2.totalValue = p.totVal.ToString();
                    //    sellBymes.Add(recordSum2);
                    //}
                    var sellBymes = new List<SellsByme>();
                    var sql3 = "select *,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares<0";
                    using (SqlConnection connection3 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd3 = new SqlCommand(sql3, connection3);
                        cmd3.CommandType = CommandType.Text;
                        connection3.Open();
                        SqlDataReader rdr3 = cmd3.ExecuteReader();
                        while (rdr3.Read())
                        {
                            SellsByme recordSum2 = new SellsByme();
                            recordSum2.company = rdr3["Company"].ToString();
                            recordSum2.volume = rdr3["Shares"].ToString();
                            recordSum2.price = Force4DecimalPlaces(rdr3["TradePrice"].ToString());
                            recordSum2.totalValue = Force4DecimalPlaces((decimal.Parse(rdr3["TradePrice"].ToString()) * decimal.Parse(rdr3["Shares"].ToString())).ToString());
                            sellBymes.Add(recordSum2);
                        }
                    }
                    recordSummary.SellDetail = sellBymes;
                    //get my sells

                    myPortf.Add(recordSummary);
                    ix = ix + 1;
                }
            }
            return Json(myPortf, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMyPortFolioNew(string cdsNumber = null)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var myPortf = new List<MyPortfolioNew>();

            //var sql = "select  from cdsc.dbo.PortfolioAll d where d.CDS_Number='" + cdsNumber + "'";

            var sql = "select d.*, ut.fnam as fullCompanyName from cdsc.dbo.PortfolioAll d,testcds_ROUTER.dbo.para_company ut  where d.CDS_Number='" + cdsNumber + "' and d.Company = ut.Company";
            var ix = 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    //Company CDS_Number  LastAcDate totAllShares    prevdayQuantity currePrice  PrevPrice Uncleared   Net
                    //prev_numbershares
                    MyPortfolioNew recordSummary = new MyPortfolioNew();
                    string curr_Port = Force4DecimalPlaces(Math.Round((double.Parse(rdr["currePrice"].ToString()) * double.Parse(rdr["totAllShares"].ToString())), 4).ToString());
                    string prev_Port = Force4DecimalPlaces(Math.Round((double.Parse(rdr["PrevPrice"].ToString()) * double.Parse(rdr["prevdayQuantity"].ToString())), 4).ToString());
                    double ret_Port = Math.Round(double.Parse(curr_Port), 4) - Math.Round(double.Parse(prev_Port), 4);
                    recordSummary.id = ix.ToString();
                    recordSummary.counter = rdr["Company"].ToString();
                    recordSummary.numbershares = rdr["totAllShares"].ToString();
                    recordSummary.prev_numbershares = rdr["prevdayQuantity"].ToString();
                    recordSummary.lastactivitydate = rdr["LastAcDate"].ToString();
                    recordSummary.currentprice = Force4DecimalPlaces(rdr["currePrice"].ToString());
                    recordSummary.prevprice = Force4DecimalPlaces(rdr["PrevPrice"].ToString());
                    recordSummary.totalportvalue = curr_Port;
                    recordSummary.totalPrevPortValue = prev_Port;
                    recordSummary.returns = Force4DecimalPlaces(ret_Port.ToString());
                    recordSummary.uncleared = Force4DecimalPlaces(rdr["Uncleared"].ToString());
                    recordSummary.net = Force4DecimalPlaces(rdr["Net"].ToString());
                    recordSummary.companyFullName = rdr["fullCompanyName"].ToString();

                    var BuysBymes = new List<BuysByme>();
                    var sql2 = "select ,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares>0";
                    using (SqlConnection connection2 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd2 = new SqlCommand(sql2, connection2);
                        cmd2.CommandType = CommandType.Text;
                        connection2.Open();
                        SqlDataReader rdr2 = cmd2.ExecuteReader();
                        while (rdr2.Read())
                        {
                            BuysByme recordSum1 = new BuysByme();
                            recordSum1.company = rdr2["Company"].ToString();
                            recordSum1.volume = rdr2["Shares"].ToString();
                            recordSum1.price = Force4DecimalPlaces(rdr2["TradePrice"].ToString());
                            recordSum1.totalValue = Force4DecimalPlaces((decimal.Parse(rdr2["TradePrice"].ToString()) * decimal.Parse(rdr2["Shares"].ToString())).ToString());
                            BuysBymes.Add(recordSum1);
                        }
                    }
                    recordSummary.BuyDetail = BuysBymes;
                    var sellBymes = new List<SellsByme>();
                    var sql3 = "select ,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares<0";
                    using (SqlConnection connection3 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd3 = new SqlCommand(sql3, connection3);
                        cmd3.CommandType = CommandType.Text;
                        connection3.Open();
                        SqlDataReader rdr3 = cmd3.ExecuteReader();
                        while (rdr3.Read())
                        {
                            SellsByme recordSum2 = new SellsByme();
                            recordSum2.company = rdr3["Company"].ToString();
                            recordSum2.volume = rdr3["Shares"].ToString();
                            recordSum2.price = Force4DecimalPlaces(rdr3["TradePrice"].ToString());
                            recordSum2.totalValue = Force4DecimalPlaces((decimal.Parse(rdr3["TradePrice"].ToString()) * decimal.Parse(rdr3["Shares"].ToString())).ToString());
                            sellBymes.Add(recordSum2);
                        }
                    }
                    recordSummary.SellDetail = sellBymes;
                    //get my sells

                    myPortf.Add(recordSummary);
                    ix = ix + 1;
                }
            }
            return Json(myPortf, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMyPortFolio(string cdsNumber = null)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var myPortf = new List<MyPortfolioNew>();

            var sql = "select d.*, ut.fnam as fullCompanyName from cdsc.dbo.PortfolioAll d,testcds_ROUTER.dbo.para_company ut  where d.CDS_Number='" + cdsNumber + "' and d.Company = ut.Company";
            var ix = 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MyPortfolioNew recordSummary = new MyPortfolioNew();
                    string curr_Port = Force4DecimalPlaces(Math.Round((double.Parse(rdr["currePrice"].ToString()) * double.Parse(rdr["totAllShares"].ToString())), 4).ToString());
                    string prev_Port = Force4DecimalPlaces(Math.Round((double.Parse(rdr["PrevPrice"].ToString()) * double.Parse(rdr["prevdayQuantity"].ToString())), 4).ToString());
                    double ret_Port = Math.Round(double.Parse(curr_Port), 4) - Math.Round(double.Parse(prev_Port), 4);
                    recordSummary.id = ix.ToString();
                    recordSummary.counter = rdr["Company"].ToString();
                    recordSummary.numbershares = rdr["totAllShares"].ToString();
                    recordSummary.prev_numbershares = rdr["prevdayQuantity"].ToString();
                    recordSummary.lastactivitydate = rdr["LastAcDate"].ToString();
                    recordSummary.currentprice = Force4DecimalPlaces(rdr["currePrice"].ToString());
                    recordSummary.prevprice = Force4DecimalPlaces(rdr["PrevPrice"].ToString());
                    recordSummary.totalportvalue = curr_Port;
                    recordSummary.totalPrevPortValue = prev_Port;
                    recordSummary.returns = Force4DecimalPlaces(ret_Port.ToString());
                    recordSummary.uncleared = Force4DecimalPlaces(rdr["Uncleared"].ToString());
                    recordSummary.net = Force4DecimalPlaces(rdr["Net"].ToString());
                    recordSummary.companyFullName = rdr["fullCompanyName"].ToString();



                    var BuysBymes = new List<BuysByme>();
                    var sql2 = "select *,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares>0";
                    using (SqlConnection connection2 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd2 = new SqlCommand(sql2, connection2);
                        cmd2.CommandType = CommandType.Text;
                        connection2.Open();
                        SqlDataReader rdr2 = cmd2.ExecuteReader();
                        while (rdr2.Read())
                        {
                            BuysByme recordSum1 = new BuysByme();
                            recordSum1.company = rdr2["Company"].ToString();
                            recordSum1.volume = rdr2["Shares"].ToString();
                            recordSum1.price = Force4DecimalPlaces(rdr2["TradePrice"].ToString());
                            recordSum1.totalValue = Force4DecimalPlaces((decimal.Parse(rdr2["TradePrice"].ToString()) * decimal.Parse(rdr2["Shares"].ToString())).ToString());
                            BuysBymes.Add(recordSum1);
                        }
                    }
                    // foreach (var p in retBids)
                    //{

                    //}
                    recordSummary.BuyDetail = BuysBymes;
                    //get my buys

                    //get my sells
                    //var retOffers = _cdDataContext.trans.Where(x => x.CDS_Number == cdsNumber.ToString() && x.Shares < 0 && x.Company == recordSummary.counter).Select(v => new
                    //{
                    //    comp = v.Company,
                    //    volum = v.Shares,
                    //    pric = v.Shares,
                    //    totVal = v.Shares
                    //});
                    //var sellBymes = new List<SellsByme>();

                    //foreach (var p in retOffers)
                    //{
                    //    SellsByme recordSum2 = new SellsByme();
                    //    recordSum2.company = p.comp.ToString();
                    //    recordSum2.volume = p.volum.ToString();
                    //    recordSum2.price = p.pric.ToString();
                    //    recordSum2.totalValue = p.totVal.ToString();
                    //    sellBymes.Add(recordSum2);
                    //}
                    var sellBymes = new List<SellsByme>();
                    var sql3 = "select *,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares<0";
                    using (SqlConnection connection3 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd3 = new SqlCommand(sql3, connection3);
                        cmd3.CommandType = CommandType.Text;
                        connection3.Open();
                        SqlDataReader rdr3 = cmd3.ExecuteReader();
                        while (rdr3.Read())
                        {
                            SellsByme recordSum2 = new SellsByme();
                            recordSum2.company = rdr3["Company"].ToString();
                            recordSum2.volume = rdr3["Shares"].ToString();
                            recordSum2.price = Force4DecimalPlaces(rdr3["TradePrice"].ToString());
                            recordSum2.totalValue = Force4DecimalPlaces((decimal.Parse(rdr3["TradePrice"].ToString()) * decimal.Parse(rdr3["Shares"].ToString())).ToString());
                            sellBymes.Add(recordSum2);
                        }
                    }
                    recordSummary.SellDetail = sellBymes;
                    //get my sells

                    myPortf.Add(recordSummary);
                    ix = ix + 1;
                }
            }
            return Json(myPortf, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getMyPortFolio1(string cdsNumber = null)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var myPortf = new List<MyPortfolio>();
            var sql = "select d.*, ut.fnam as fullCompanyName from cdsc.dbo.portfolioAll d,testcds_ROUTER.dbo.para_company ut  where d.CDS_Number='" + cdsNumber + "' and d.Company = ut.Company";
            var ix = 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    var Mytotalportvalue = Double.Parse(rdr["currePrice"].ToString()) * Double.Parse(rdr["totAllShares"].ToString());
                    var MytotalPrevPortValue = Double.Parse(rdr["PrevPrice"].ToString()) * Double.Parse(rdr["prevdayQuantity"].ToString());
                    var retVal = Mytotalportvalue - MytotalPrevPortValue;
                    MyPortfolio recordSummary = new MyPortfolio();
                    recordSummary.id = ix.ToString();
                    recordSummary.counter = rdr["Company"].ToString();
                    recordSummary.numbershares = rdr["totAllShares"].ToString();
                    recordSummary.lastactivitydate = rdr["LastAcDate"].ToString();
                    recordSummary.currentprice = Force4DecimalPlaces(rdr["currePrice"].ToString());
                    recordSummary.prevprice = Force4DecimalPlaces(rdr["PrevPrice"].ToString());
                    recordSummary.totalportvalue = Force4DecimalPlaces(Mytotalportvalue.ToString());
                    recordSummary.uncleared = Force4DecimalPlaces(rdr["Uncleared"].ToString());
                    recordSummary.net = Force4DecimalPlaces(rdr["Net"].ToString());
                    recordSummary.totalPrevPortValue = Force4DecimalPlaces(MytotalPrevPortValue.ToString());
                    recordSummary.companyFullName = rdr["fullCompanyName"].ToString();
                    recordSummary.returns = Force4DecimalPlaces(retVal.ToString());
                    //get my buys
                    ////var retBids = _cdDataContext.trans.Where(x => x.CDS_Number == cdsNumber.ToString() && x.Shares > 0 && x.Company == recordSummary.counter).Select(v => new
                    ////{
                    ////    comp = v.Company,
                    ////    volum = v.Shares,
                    ////    pric = v.Shares,
                    ////    totVal = v.Shares
                    ////});
                    var BuysBymes = new List<BuysByme>();
                    var sql2 = "select *,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares>0";
                    using (SqlConnection connection2 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd2 = new SqlCommand(sql2, connection2);
                        cmd2.CommandType = CommandType.Text;
                        connection2.Open();
                        SqlDataReader rdr2 = cmd2.ExecuteReader();
                        while (rdr2.Read())
                        {
                            BuysByme recordSum1 = new BuysByme();
                            recordSum1.company = rdr2["Company"].ToString();
                            recordSum1.volume = rdr2["Shares"].ToString();
                            recordSum1.price = Force4DecimalPlaces(rdr2["TradePrice"].ToString());
                            recordSum1.totalValue = Force4DecimalPlaces((decimal.Parse(rdr2["TradePrice"].ToString()) * decimal.Parse(rdr2["Shares"].ToString())).ToString());
                            BuysBymes.Add(recordSum1);
                        }
                    }
                    // foreach (var p in retBids)
                    //{

                    //}
                    recordSummary.BuyDetail = BuysBymes;
                    //get my buys

                    //get my sells
                    //var retOffers = _cdDataContext.trans.Where(x => x.CDS_Number == cdsNumber.ToString() && x.Shares < 0 && x.Company == recordSummary.counter).Select(v => new
                    //{
                    //    comp = v.Company,
                    //    volum = v.Shares,
                    //    pric = v.Shares,
                    //    totVal = v.Shares
                    //});
                    //var sellBymes = new List<SellsByme>();

                    //foreach (var p in retOffers)
                    //{
                    //    SellsByme recordSum2 = new SellsByme();
                    //    recordSum2.company = p.comp.ToString();
                    //    recordSum2.volume = p.volum.ToString();
                    //    recordSum2.price = p.pric.ToString();
                    //    recordSum2.totalValue = p.totVal.ToString();
                    //    sellBymes.Add(recordSum2);
                    //}
                    var sellBymes = new List<SellsByme>();
                    var sql3 = "select *,ISNULL((SELECT TOP (1) TradePrice FROM CDS_ROUTER.dbo.tblMatchedOrders AS q WHERE ((ReportID = d.Reference) AND (Account1 = d.CDS_Number)) OR ((ReportID = d.Reference) AND (Account2 = d.CDS_Number))),1) AS TradePrice from cds_router.dbo.trans d where d.CDS_Number='" + cdsNumber + "' and d.Company='" + recordSummary.counter + "' and d.Shares<0";
                    using (SqlConnection connection3 = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd3 = new SqlCommand(sql3, connection3);
                        cmd3.CommandType = CommandType.Text;
                        connection3.Open();
                        SqlDataReader rdr3 = cmd3.ExecuteReader();
                        while (rdr3.Read())
                        {
                            SellsByme recordSum2 = new SellsByme();
                            recordSum2.company = rdr3["Company"].ToString();
                            recordSum2.volume = rdr3["Shares"].ToString();
                            recordSum2.price = Force4DecimalPlaces(rdr3["TradePrice"].ToString());
                            recordSum2.totalValue = Force4DecimalPlaces((decimal.Parse(rdr3["TradePrice"].ToString()) * decimal.Parse(rdr3["Shares"].ToString())).ToString());
                            sellBymes.Add(recordSum2);
                        }
                    }
                    recordSummary.SellDetail = sellBymes;
                    //get my sells

                    myPortf.Add(recordSummary);
                    ix = ix + 1;
                }
            }
            return Json(myPortf, JsonRequestBehavior.AllowGet);
        }

        public string getAccountFullname(string any = null)
        {
            var getBrk = "select ISNULL(ac.Surname,'') + ' ' + ISNULL(ac.Forenames,'') as fullName from [CDS_ROUTER].[dbo].Accounts_Clients ac where ac.CDS_Number='" + any + "'";
            var theFullName = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    theFullName = rdr["fullName"].ToString();
                }
                return theFullName;
            }
        }

        public JsonResult getPortfolioStatement(string cdsNumber = null, string company = null)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["cdscDbContext"].ConnectionString;
            var myPortf = new List<MyPortStatement>();
            var addStr = "";
            if (company != null)
            {
                addStr = " and d.company='" + company + "'";
            }
            var sql = "select *,[DealShares]*[Average Price] as PurchaceValue,([Average Price]-[DEAL_PRICE])*[DealShares] as Returnss from cdsc.dbo.MyPortStatement d where d.CDS_Number='" + cdsNumber + "' " + addStr + "";
            var ix = 1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MyPortStatement recordSummary = new MyPortStatement();
                    recordSummary.id = ix.ToString();
                    recordSummary.counter = rdr["Company"].ToString();
                    recordSummary.date = rdr["LastActDate"].ToString();
                    recordSummary.pricepershare = rdr["DEAL_PRICE"].ToString();
                    recordSummary.volume = rdr["DealShares"].ToString();
                    recordSummary.purchasevalue = rdr["PurchaceValue"].ToString();
                    recordSummary.currentmarketprice = rdr["Average Price"].ToString();
                    recordSummary.returns = rdr["Returnss"].ToString();

                    myPortf.Add(recordSummary);
                    ix = ix + 1;
                }
            }
            return Json(myPortf, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGraphPrices(string company = null)
        {
            var rets = _AtsDbContext.CompanyLivePrices.Where(x => x.COMPANY == company.Trim()).OrderByDescending(x => x.id).Select(c => new
            {
                price = c.CurrentPrice,
                vol = c.ShareVOL
            }).Take(5);
            List<CompanyLivePricess> my = new List<CompanyLivePricess>();
            foreach (var p in rets)
            {
                my.Add(new CompanyLivePricess() { CurrentPrice = p.price.ToString(), currentVolume = p.vol.ToString() });
            }
            return Json(my, JsonRequestBehavior.AllowGet);
        }

        public string OrderPosting(string company, string security, string orderTrans,
          string orderType, string quantity, string price, string cdsNumber,
          string broker, string source)

        {
            var today = DateTime.Today.ToString("yyyy-MM-dd");
            var ret = "";
            var atsDbContext = new AtsDbContext();
            var cdsDbContext = new CdDataContext();
            var tempDbContext = new cdscDbContext();
            var orderPref = "";
            decimal basePrice = 0;
            var theOrderTrans = "";

            try
            {
                if (orderTrans != null && orderTrans.Trim().Equals("Buy"))
                {
                    theOrderTrans = "BUY";
                }
                else
                {
                    theOrderTrans = "SELL";
                }


                if (orderType != null && orderType.Equals("Market"))
                {
                    orderPref = "M";
                    basePrice = 0;
                }
                else
                {
                    basePrice = decimal.Parse(price);
                    orderPref = "L";
                }


                long orderNumber = 0;
                var myCompany = "";
                var myBrokerCode = "";


                var theBrokerRef = atsDbContext.Pre_Order_Live.OrderByDescending(x => x.OrderNo).FirstOrDefault();
                if (theBrokerRef != null)
                {
                    orderNumber = theBrokerRef.OrderNo + 1;
                }
                else
                {
                    orderNumber = 1;
                }

                var theCompnay =
                    atsDbContext.para_company.OrderByDescending(x => x.ID)
                        .Where(x => x.Company == company)
                        .FirstOrDefault(x => x.Company == company);

                if (theCompnay == null)
                {
                    return "Select a valid company";
                }

                myCompany = theCompnay.Company;
                var theCds = cdsNumber + "";

                //                var theBroker = cdsDbContext.Accounts_Clients.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                if (GetCdsNumber(cdsNumber) == "0" || GetCdsNumber(cdsNumber) == "1")
                {
                    return "Enter CDS Number";
                }


                var theBroker1 = cdsDbContext.Client_Companies.FirstOrDefault(x => x.Company_Code == broker);
                if (theBroker1 == null)
                {
                    return "Enter valid broker";
                }


                decimal shares = 0;
                if (orderTrans.ToString().ToUpper().Equals("SELL"))
                {
                    var shareAvail =
                        cdsDbContext.trans.FirstOrDefault(x => x.CDS_Number == cdsNumber && x.Company == myCompany);

                    if (shareAvail != null)
                    {
                        var theShare =
                            cdsDbContext.trans.Where(x => x.CDS_Number == cdsNumber && x.Company == myCompany)
                                .Select(x => x.Shares)
                                .Sum();
                        if (theShare <= 0)
                        {
                            return "You have insufficient shares, check with your broker";
                        }
                        if (theShare < int.Parse(quantity))
                        {
                            return "You have insufficient shares, check with your broker";
                        }

                    }
                    else
                    {
                        return "You have insufficient shares, check with your broker";
                    }
                }


                //IF BUY ORDER
                var totalAmountToSpent = decimal.Parse(quantity) * decimal.Parse(price) * decimal.Parse("1.01693");


                if (orderTrans.ToString().ToUpper().Equals("BUY"))
                {
                    var moneyAvail =
                        tempDbContext.CashTrans.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                    if (moneyAvail != null)
                    {
                        var theCashBal =
                            tempDbContext.CashTrans.Where(x => x.CDS_Number == cdsNumber)
                                .Select(x => x.Amount)
                                .Sum();
                        if (theCashBal <= 0 || theCashBal < totalAmountToSpent)
                        {
                            return "You have insufficient balance in your Cash account";
                        }
                    }
                    else
                    {
                        return "You have insufficient balance in your Cash account";
                    }
                }
                //SAVING TO DB


                var orderStatus = "OPEN";
                var createdDate = DateTime.Now;
                var dealBeginDate = DateTime.Now;
                var expiryDate = DateTime.Now;
                var theQuantity = 0;


                if (quantity != null)
                {
                    theQuantity = int.Parse(quantity);
                }


                var brokerCode = "";
                var orderAttrib = "";
                var marketBoard = "Normal Board";
                var timeInForce = "Day Order (DO)";
                var orderQualifier = "None";
                //var brokerRef = theBroker1.Company_Code + "" + orderNumber;
                var contraBrokerId = "";
                var brokerRef = broker + orderNumber;
                var maxPrice = 0;
                var minPrice = 0;
                var flagOldOrder = false;
                var orderNum = "MOB-" + orderNumber;
                var currency = "$";


                var name = GetCdsNumber(cdsNumber).ToUpper();

                try
                {
                    var orderLive = new Pre_Order_Live
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = broker,
                        Client_Type = "",
                        Tax = 0,
                        Shareholder = cdsNumber,
                        ClientName = name,
                        TotalShareHolding = 0,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Deal_Begin_Date = dealBeginDate,
                        Expiry_Date = expiryDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        AvailableShares = 0,
                        OrderPref = orderPref,
                        OrderAttribute = orderAttrib,
                        Marketboard = marketBoard,
                        TimeInForce = timeInForce,
                        OrderQualifier = orderQualifier,
                        BrokerRef = brokerRef,
                        ContraBrokerId = contraBrokerId,
                        MaxPrice = maxPrice,
                        MiniPrice = minPrice,
                        Flag_oldorder = flagOldOrder,
                        OrderNumber = orderNum,
                        Currency = currency,
                        trading_platform = GetTradingPlaform(myCompany),
                        FOK = false,

                        Affirmation = true
                    };
                    //save to cdsc tempPreorderLive too
                    var orderPreorderCdsc = new PreOrderLives
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = broker,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        TimeInForce = timeInForce,
                        OrderQualifier = orderQualifier,
                        BrokerRef = brokerRef,
                        OrderNumber = orderNum,
                        Source = source
                    };
                    //save to cdsc tempPreorderLive too
                    var orderCashTrans = new CashTrans
                    {
                        Description = "BUY - Order",
                        TransType = "BUY",
                        TransStatus = "1",
                        Amount = -totalAmountToSpent,
                        CDS_Number = cdsNumber,
                        DateCreated = DateTime.Now
                    };

                    try
                    {
                        //                      var emailAdd = theBroker.Email;
                        atsDbContext.Pre_Order_Live.Add(orderLive);
                        atsDbContext.SaveChanges();
                        //                      SendMail2(emailAdd, "Your order was successfully posted", "Order Posting");
                        tempDbContext.PreOrderLives.Add(orderPreorderCdsc);
                        tempDbContext.SaveChanges();

                        if (orderTrans.ToString().ToUpper().Equals("BUY"))
                        {
                            tempDbContext.CashTrans.Add(orderCashTrans);
                            tempDbContext.SaveChanges();
                        }

                        return "1";
                    }
                    catch (Exception e)
                    {
                        return "Error Occured trying to send order please try again" + e;
                    }
                }
                catch (Exception e)
                {
                    return "Assigning values => " + e.ToString();
                }

            }
            catch (Exception ex)
            {
                return "All errors => " + ex.ToString();
            }
        }


        public string GetBrokerFromCDS(string cds)
        {
            var getBrk =
                "SELECT TOP 1 BrokerCode FROM Accounts_Clients WHERE CDS_Number = '" +
                cds.Trim() + "'";

            var ret = "";

            using (var connection = new SqlConnection(connectionString))
            {
                var cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                var rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        ret += rdr["BrokerCode"];
                    }
                    try
                    {
                        return ret;
                    }
                    catch (Exception)
                    {
                        return "1";
                    }
                }
                return "0";
            }
        }


        public string OrderPostingMobile(string company, string security, string orderTrans,
          string orderType, string quantity, string price, string cdsNumber,
          string broker, string source)

        {
            var today = DateTime.Today.ToString("yyyy-MM-dd");
            var ret = "";
            var atsDbContext = new AtsDbContext();
            var cdsDbContext = new CdDataContext();
            var tempDbContext = new cdscDbContext();
            var orderPref = "";
            decimal basePrice = 0;
            var theOrderTrans = "";

            try
            {
                if (orderTrans != null && orderTrans.Trim().Equals("Buy"))
                {
                    theOrderTrans = "BUY";
                }
                else
                {
                    theOrderTrans = "SELL";
                }


                if (orderType != null && orderType.Equals("Market"))
                {
                    orderPref = "M";
                    basePrice = 0;
                }
                else
                {
                    basePrice = decimal.Parse(price);
                    orderPref = "L";
                }


                long orderNumber = 0;
                var myCompany = "";
                var myBrokerCode = "";


                var theBrokerRef = atsDbContext.Pre_Order_Live.OrderByDescending(x => x.OrderNo).FirstOrDefault();
                if (theBrokerRef != null)
                {
                    orderNumber = theBrokerRef.OrderNo + 1;
                }
                else
                {
                    orderNumber = 1;
                }

                var theCompnay =
                    atsDbContext.para_company.OrderByDescending(x => x.ID)
                        .Where(x => x.fnam == company)
                        .FirstOrDefault(x => x.fnam == company);

                if (theCompnay == null)
                {
                    return "Select a valid company";
                }

                myCompany = theCompnay.Company;

                var theCds = cdsNumber + "";

                var theBroker = cdsDbContext.Accounts_Clients.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                if (GetCdsNumber(cdsNumber) == "0" || GetCdsNumber(cdsNumber) == "1")
                {
                    return "Enter CDS Number";
                }


                var theBroker1 = cdsDbContext.Client_Companies.FirstOrDefault(x => x.Company_Code == broker);
                //if (theBroker1 == null)
                //{
                //    return "Enter valid broker";
                //}


                decimal shares = 0;
                if (orderTrans.ToString().ToUpper().Equals("SELL"))
                {
                    var shareAvail =
                        cdsDbContext.trans.FirstOrDefault(x => x.CDS_Number == cdsNumber && x.Company == myCompany);

                    if (shareAvail != null)
                    {
                        var theShare = decimal.Parse(getCurrentBalance(company, cdsNumber));
                        //cdsDbContext.trans.Where(x => x.CDS_Number == cdsNumber && x.Company == myCompany)
                        //    .Select(x => x.Shares)
                        //    .Sum();
                        if (theShare <= 0)
                        {
                            return "You have insufficient shares, check with your broker";
                        }
                        if (theShare < int.Parse(quantity))
                        {
                            return "You have insufficient shares, check with your broker";
                        }

                    }
                    else
                    {
                        return "You have insufficient shares, check with your broker";
                    }
                }


                //IF BUY ORDER
                var totalAmountToSpent = decimal.Parse(quantity) * decimal.Parse(price) * decimal.Parse("1.01693");
                if (orderTrans.ToString().ToUpper().Equals("BUY"))
                {
                    var moneyAvail =
                        tempDbContext.CashTrans.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                    if (moneyAvail != null)
                    {
                        var theCashBal =
                            tempDbContext.CashTrans.Where(x => x.CDS_Number == cdsNumber)
                                .Select(x => x.Amount)
                                .Sum();
                        if (theCashBal <= 0 || theCashBal < totalAmountToSpent)
                        {
                            return "You have insufficient balance in your Cash account";
                        }
                    }
                    else
                    {
                        return "You have insufficient balance in your Cash account";
                    }
                }
                //SAVING TO DB


                var orderStatus = "OPEN";
                var createdDate = DateTime.Now;
                var dealBeginDate = DateTime.Now;
                var expiryDate = DateTime.Now;
                var theQuantity = 0;


                if (quantity != null)
                {
                    theQuantity = int.Parse(quantity);
                }


                var brokerCode = "";
                var orderAttrib = "";
                var marketBoard = "Normal Board";
                var timeInForce = "Day Order (DO)";
                var brokerRef = broker + orderNumber;
                var contraBrokerId = "";
                var maxPrice = 0;
                var minPrice = 0;
                var flagOldOrder = false;
                var orderNum = "MOB-" + orderNumber;
                var currency = "$";


                var name = GetCdsNumber(cdsNumber).ToUpper();

                try
                {
                    var orderLive = new Pre_Order_Live
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = broker,
                        Client_Type = "",
                        Tax = 0,
                        Shareholder = cdsNumber,
                        ClientName = name,
                        TotalShareHolding = 0,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Deal_Begin_Date = dealBeginDate,
                        Expiry_Date = expiryDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        AvailableShares = 0,
                        OrderPref = orderPref,
                        OrderAttribute = orderAttrib,
                        Marketboard = marketBoard,
                        TimeInForce = timeInForce,
                        OrderQualifier = "None",
                        BrokerRef = brokerRef,
                        ContraBrokerId = contraBrokerId,
                        MaxPrice = maxPrice,
                        MiniPrice = minPrice,
                        Flag_oldorder = flagOldOrder,
                        OrderNumber = orderNum,
                        Currency = currency,
                        FOK = false,
                        trading_platform = GetTradingPlaform(myCompany),
                        Affirmation = true
                    };
                    //save to cdsc tempPreorderLive too
                    var orderPreorderCdsc = new PreOrderLives
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = broker,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        TimeInForce = timeInForce,
                        OrderQualifier = "None",
                        BrokerRef = brokerRef,
                        OrderNumber = orderNum,
                        Source = source
                    };
                    //save to cdsc tempPreorderLive too
                    var orderCashTrans = new CashTrans
                    {
                        Description = "BUY - Order",
                        TransType = "BUY",
                        TransStatus = "1",
                        Amount = -totalAmountToSpent,
                        CDS_Number = cdsNumber,
                        DateCreated = DateTime.Now
                    };

                    try
                    {
                        //                      var emailAdd = theBroker.Email;
                        atsDbContext.Pre_Order_Live.Add(orderLive);
                        atsDbContext.SaveChanges();
                        //                      SendMail2(emailAdd, "Your order was successfully posted", "Order Posting");

                        /*
                        *Saving to CDSC Table 
                        */
                        tempDbContext.PreOrderLives.Add(orderPreorderCdsc);
                        tempDbContext.SaveChanges();

                        if (orderTrans.ToString().ToUpper().Equals("BUY"))
                        {
                            tempDbContext.CashTrans.Add(orderCashTrans);
                            tempDbContext.SaveChanges();
                        }

                        return "Your Order has been successfully placed";
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }
                catch (Exception e)
                {
                    return "Assigning values => " + e.ToString();
                }

            }
            catch (Exception ex)
            {
                return "All errors => " + ex.ToString();
            }
        }

        public string OrderPostingMobile1(string company, string security, string orderTrans,
          string orderType, string quantity, string price, string cdsNumber,
          string broker, string expiresOn, string source)

        {
            var today = DateTime.Today.ToString("yyyy-MM-dd");
            var ret = "";
            var atsDbContext = new AtsDbContext();
            var cdsDbContext = new CdDataContext();
            var tempDbContext = new cdscDbContext();
            var orderPref = "";
            decimal basePrice = 0;
            var theOrderTrans = "";

            try
            {
                if (orderTrans != null && orderTrans.Trim().Equals("Buy"))
                {
                    theOrderTrans = "BUY";
                }
                else
                {
                    theOrderTrans = "SELL";
                }


                if (orderType != null && orderType.Equals("Market"))
                {
                    orderPref = "M";
                    basePrice = 0;
                }
                else
                {
                    basePrice = decimal.Parse(price);
                    orderPref = "L";
                }


                long orderNumber = 0;
                var myCompany = "";
                var myBrokerCode = "";


                var theBrokerRef = atsDbContext.Pre_Order_Live.OrderByDescending(x => x.OrderNo).FirstOrDefault();
                if (theBrokerRef != null)
                {
                    orderNumber = theBrokerRef.OrderNo + 1;
                }
                else
                {
                    orderNumber = 1;
                }

                var theCompnay =
                    atsDbContext.para_company.OrderByDescending(x => x.ID)
                        .Where(x => x.fnam == company)
                        .FirstOrDefault(x => x.fnam == company);

                if (theCompnay == null)
                {
                    return "Select a valid company";
                }

                myCompany = theCompnay.Company;

                var theCds = cdsNumber + "";

                var theBroker = cdsDbContext.Accounts_Clients.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                if (GetCdsNumber(cdsNumber) == "0" || GetCdsNumber(cdsNumber) == "1")
                {
                    return "Enter CDS Number";
                }


                var theBroker1 = cdsDbContext.Client_Companies.FirstOrDefault(x => x.Company_Code == broker);
                //if (theBroker1 == null)
                //{
                //    return "Enter valid broker";
                //}


                decimal shares = 0;
                if (orderTrans.ToString().ToUpper().Equals("SELL"))
                {
                    var shareAvail =
                        cdsDbContext.trans.FirstOrDefault(x => x.CDS_Number == cdsNumber && x.Company == myCompany);

                    if (shareAvail != null)
                    {
                        var theShare =
                            cdsDbContext.trans.Where(x => x.CDS_Number == cdsNumber && x.Company == myCompany)
                                .Select(x => x.Shares)
                                .Sum();
                        if (theShare <= 0)
                        {
                            return "You have insufficient shares, check with your broker";
                        }
                        if (theShare < int.Parse(quantity))
                        {
                            return "You have insufficient shares, check with your broker";
                        }

                    }
                    else
                    {
                        return "You have insufficient shares, check with your broker";
                    }
                }


                //IF BUY ORDER
                var totalAmountToSpent = decimal.Parse(quantity) * decimal.Parse(price) * decimal.Parse("1.01693");
                if (orderTrans.ToString().ToUpper().Equals("BUY"))
                {
                    var moneyAvail =
                        tempDbContext.CashTrans.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                    if (moneyAvail != null)
                    {
                        var theCashBal =
                            tempDbContext.CashTrans.Where(x => x.CDS_Number == cdsNumber)
                                .Select(x => x.Amount)
                                .Sum();
                        if (theCashBal <= 0 || theCashBal < totalAmountToSpent)
                        {
                            return "You have insufficient balance in your Cash account";
                        }
                    }
                    else
                    {
                        return "You have insufficient balance in your Cash account";
                    }
                }
                //SAVING TO DB


                var orderStatus = "OPEN";
                var createdDate = DateTime.Now;
                var dealBeginDate = DateTime.Now;
                //var expiryDate =  DateTime.Now; 
                var expiryDate = DateTime.Now;
                DateTime testExpiryDate;

                if ((expiresOn != null || expiresOn != "") && (DateTime.TryParse(expiresOn, out testExpiryDate)))//if expires on date has been provided
                {
                    //expiryDate = Convert.ToDateTime(expiresOn);
                    expiryDate = DateTime.Parse(expiresOn);
                }
                else
                {
                    expiryDate = DateTime.Now;
                }

                var theQuantity = 0;


                if (quantity != null)
                {
                    theQuantity = int.Parse(quantity);
                }


                var brokerCode = "";
                var orderAttrib = "";
                var marketBoard = "Normal Board";
                var timeInForce = "Day Order (DO)";
                var brokerRef = broker + orderNumber;
                var contraBrokerId = "";
                var maxPrice = 0;
                var minPrice = 0;
                var flagOldOrder = false;
                var orderNum = "MOB-" + orderNumber;
                var currency = "$";


                var name = GetCdsNumber(cdsNumber).ToUpper();

                try
                {
                    var orderLive = new Pre_Order_Live
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = broker,
                        Client_Type = "",
                        Tax = 0,
                        Shareholder = cdsNumber,
                        ClientName = name,
                        TotalShareHolding = 0,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Deal_Begin_Date = dealBeginDate,
                        Expiry_Date = expiryDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        AvailableShares = 0,
                        OrderPref = orderPref,
                        OrderAttribute = orderAttrib,
                        Marketboard = marketBoard,
                        TimeInForce = timeInForce,
                        OrderQualifier = "None",
                        BrokerRef = brokerRef,
                        ContraBrokerId = contraBrokerId,
                        MaxPrice = maxPrice,
                        MiniPrice = minPrice,
                        Flag_oldorder = flagOldOrder,
                        OrderNumber = orderNum,
                        Currency = currency,
                        FOK = false,
                        trading_platform = GetTradingPlaform(myCompany),
                        Affirmation = true
                    };
                    //save to cdsc tempPreorderLive too
                    var orderPreorderCdsc = new PreOrderLives
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = broker,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        TimeInForce = timeInForce,
                        OrderQualifier = "None",
                        BrokerRef = brokerRef,
                        OrderNumber = orderNum,
                        Source = source
                    };
                    //save to cdsc tempPreorderLive too
                    var orderCashTrans = new CashTrans
                    {
                        Description = "BUY - Order",
                        TransType = "BUY",
                        TransStatus = "1",
                        Amount = -totalAmountToSpent,
                        CDS_Number = cdsNumber,
                        DateCreated = DateTime.Now
                    };

                    try
                    {
                        //                      var emailAdd = theBroker.Email;
                        atsDbContext.Pre_Order_Live.Add(orderLive);
                        atsDbContext.SaveChanges();
                        //                      SendMail2(emailAdd, "Your order was successfully posted", "Order Posting");

                        /*
                        *Saving to CDSC Table 
                        */
                        tempDbContext.PreOrderLives.Add(orderPreorderCdsc);
                        tempDbContext.SaveChanges();

                        if (orderTrans.ToString().ToUpper().Equals("BUY"))
                        {
                            tempDbContext.CashTrans.Add(orderCashTrans);
                            tempDbContext.SaveChanges();
                        }

                        return "Your Order has been successfully placed";
                    }
                    catch (Exception e)
                    {
                        //return e.ToString();
                        return "Timeout, please check your Internet connection.";
                    }
                }
                catch (Exception e)
                {
                    return "An error has occured in posting your order, please try again.";
                }

            }
            catch (Exception ex)
            {
                return "An error has occured in posting your order, please try again.";
            }
        }


        public string OrderPostingIPO(string company, string security, string orderTrans,
          string orderType, string quantity, string price, string cdsNumber,
          string broker, string source)

        {
            var tot = Math.Round(decimal.Parse(price) * int.Parse(quantity), 2);
            var today = DateTime.Today.ToString("yyyy-MM-dd");
            var ret = "";
            var atsDbContext = new AtsDbContext();
            var cdsDbContext = new CdDataContext();
            var tempDbContext = new cdscDbContext();
            var orderPref = "";
            decimal basePrice = 0;
            var theOrderTrans = "";

            try
            {
                if (orderTrans != null && orderTrans.Trim().Equals("Buy"))
                {
                    theOrderTrans = "BUY";
                }
                else
                {
                    theOrderTrans = "SELL";
                }


                if (orderType != null && orderType.Equals("Market"))
                {
                    orderPref = "M";
                    basePrice = 0;
                }
                else
                {
                    basePrice = decimal.Parse(price);
                    orderPref = "L";
                }


                long orderNumber = 0;
                var myCompany = "";
                var myBrokerCode = "";


                var theBrokerRef = atsDbContext.Pre_Order_Live.OrderByDescending(x => x.OrderNo).FirstOrDefault();
                if (theBrokerRef != null)
                {
                    orderNumber = theBrokerRef.OrderNo + 1;
                }
                else
                {
                    orderNumber = 1;
                }

                //var theCompnay =
                //    atsDbContext.para_company.OrderByDescending(x => x.ID)
                //        .Where(x => x.Company == company)
                //        .FirstOrDefault(x => x.Company == company);

                //if (theCompnay == null)
                //{
                //    return "Select a valid company";
                //}

                myCompany = company;
                var theCds = cdsNumber + "";

                //                var theBroker = cdsDbContext.Accounts_Clients.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                if (GetCdsNumber(cdsNumber) == "0" || GetCdsNumber(cdsNumber) == "1")
                {
                    return "Enter CDS Number";
                }


                var theBroker1 = cdsDbContext.Client_Companies.FirstOrDefault(x => x.Company_Code == broker);
                if (theBroker1 == null)
                {
                    return "Enter valid broker";
                }

                var returnVal = LIMITS_CHECK(myCompany, cdsNumber, quantity);
                if (returnVal != "0")
                {
                    return returnVal;
                }

                decimal shares = 0;
                if (orderTrans.ToString().ToUpper().Equals("SELL"))
                {
                    var shareAvail =
                        cdsDbContext.trans.FirstOrDefault(x => x.CDS_Number == cdsNumber && x.Company == myCompany);

                    if (shareAvail != null)
                    {
                        var theShare =
                            cdsDbContext.trans.Where(x => x.CDS_Number == cdsNumber && x.Company == myCompany)
                                .Select(x => x.Shares)
                                .Sum();
                        if (theShare <= 0)
                        {
                            return "You have insufficient shares, check with your broker";
                        }
                    }
                    else
                    {
                        return "You have insufficient shares, check with your broker";
                    }
                }


                //IF BUY ORDER
                var totalAmountToSpent = decimal.Parse(quantity) * decimal.Parse(price);
                if (orderTrans.ToString().ToUpper().Equals("BUY"))
                {
                    var moneyAvail =
                        tempDbContext.CashTrans.FirstOrDefault(x => x.CDS_Number == cdsNumber);

                    if (moneyAvail != null)
                    {
                        var theCashBal =
                            tempDbContext.CashTrans.Where(x => x.CDS_Number == cdsNumber)
                                .Select(x => x.Amount)
                                .Sum();
                        if (theCashBal <= 0 || theCashBal < totalAmountToSpent)
                        {
                            return "You have insufficient balance in your Cash account";
                        }
                    }
                    else
                    {
                        return "You have insufficient balance in your Cash account";
                    }
                }
                //SAVING TO DB


                var orderStatus = "OPEN";
                var createdDate = DateTime.Now;
                var dealBeginDate = DateTime.Now;
                var expiryDate = DateTime.Now;
                var theQuantity = 0;


                if (quantity != null)
                {
                    theQuantity = int.Parse(quantity);
                }


                var brokerCode = "";
                var orderAttrib = "";
                var marketBoard = "Normal Board";
                var timeInForce = "Day Order (DO)";
                var orderQualifier = "None";
                var brokerRef = broker + orderNumber;
                var contraBrokerId = "";
                var maxPrice = 0;
                var minPrice = 0;
                var flagOldOrder = false;
                var orderNum = "MOB-" + orderNumber;
                var currency = "USD";


                var name = GetCdsNumber(cdsNumber).ToUpper();

                try
                {
                    var orderLive = new Pre_Order_Live
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = theBroker1.Company_Code,
                        Client_Type = "",
                        Tax = 0,
                        Shareholder = cdsNumber,
                        ClientName = name,
                        TotalShareHolding = 0,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Deal_Begin_Date = dealBeginDate,
                        Expiry_Date = expiryDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        //                                                                    AvailableShares = availableShares,
                        OrderPref = orderPref,
                        OrderAttribute = orderAttrib,
                        Marketboard = marketBoard,
                        TimeInForce = timeInForce,
                        OrderQualifier = orderQualifier,
                        BrokerRef = brokerRef,
                        ContraBrokerId = contraBrokerId,
                        MaxPrice = maxPrice,
                        MiniPrice = minPrice,
                        Flag_oldorder = flagOldOrder,
                        OrderNumber = orderNum,
                        Currency = currency,
                        FOK = false,
                        Affirmation = true
                    };
                    //save to cdsc tempPreorderLive too
                    var orderPreorderCdsc = new PreOrderLivesIPOes
                    {
                        OrderType = orderTrans.ToUpper(),
                        Company = myCompany,
                        SecurityType = security,
                        CDS_AC_No = cdsNumber,
                        Broker_Code = theBroker1.Company_Code,
                        OrderStatus = orderStatus,
                        Create_date = createdDate,
                        Quantity = theQuantity,
                        BasePrice = basePrice,
                        TimeInForce = timeInForce,
                        OrderQualifier = orderQualifier,
                        BrokerRef = brokerRef,
                        OrderNumber = orderNum,
                        Source = source
                    };

                    //save to cdsc tempPreorderLive too
                    var orderCashTrans = new CashTrans
                    {
                        Description = "BUY - IPO",
                        TransType = "BUY",
                        TransStatus = "1",
                        Amount = -totalAmountToSpent,
                        CDS_Number = cdsNumber,
                        DateCreated = DateTime.Now
                    };
                    try
                    {
                        //                      var emailAdd = theBroker.Email;
                        //atsDbContext.Pre_Order_Live.Add(orderLive);
                        //atsDbContext.SaveChanges();
                        //                      SendMail2(emailAdd, "Your order was successfully posted", "Order Posting");
                        tempDbContext.PreOrderLivesIPOes.Add(orderPreorderCdsc);
                        tempDbContext.SaveChanges();

                        tempDbContext.CashTrans.Add(orderCashTrans);
                        tempDbContext.SaveChanges();

                        var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            SqlCommand cmdn = new SqlCommand();
                            cmdn.Connection = connection;
                            cmdn.CommandType = CommandType.StoredProcedure;
                            cmdn.CommandText = "AReceiveBidsIPO";
                            cmdn.Parameters.AddWithValue("@Bank", "0");
                            cmdn.Parameters.AddWithValue("@Branch", "0");
                            cmdn.Parameters.AddWithValue("@Accountnumber", "0");
                            cmdn.Parameters.AddWithValue("@No_of_Notes_Applied", quantity);
                            cmdn.Parameters.AddWithValue("@AmountPaid", tot);
                            cmdn.Parameters.AddWithValue("@PaymentRefNo", "Escrow");
                            cmdn.Parameters.AddWithValue("@ClientType", "LI");
                            cmdn.Parameters.AddWithValue("@BrokerReference", "OMSEC");
                            cmdn.Parameters.AddWithValue("@DividendDisposalPreference", "M");
                            cmdn.Parameters.AddWithValue("@MNO_", "ONLINE");
                            cmdn.Parameters.AddWithValue("@Identification", cdsNumber);
                            cmdn.Parameters.AddWithValue("@TelephoneNumber", cdsNumber);
                            cmdn.Parameters.AddWithValue("@CDSC_Number", cdsNumber);
                            cmdn.Parameters.AddWithValue("@ReceiptNumber", "Escrow");
                            cmdn.Parameters.AddWithValue("@Company", company);
                            //cmdn.Parameters.AddWithValue("@Custodian", "0");
                            //cmdn.Parameters.AddWithValue("@TransNum", "0");
                            //cmdn.Parameters.AddWithValue("@PledgeIndicator", "0");
                            //cmdn.Parameters.AddWithValue("@PledgeeBPID", cdsNumber);
                            connection.Open();
                            if (cmdn.ExecuteNonQuery() > 0)
                            {
                                connection.Close();
                                return "1";
                            }
                            else
                            {
                                connection.Close();
                                return "Error saving IPO";
                            }
                        }
                        //return "1";
                    }
                    catch (Exception e)
                    {
                        return e.ToString();
                    }
                }
                catch (Exception e)
                {
                    return "Assigning values => " + e.ToString();
                }

            }
            catch (Exception ex)
            {
                return "All errors => " + ex.ToString();
            }
        }


        public string OrderIPOPostingPOSTANDROID(string company, string quantity, string price, string cdsNumber)
        {
            var tot = Math.Round(decimal.Parse(price) * int.Parse(quantity), 2);


            var returnVal = LIMITS_CHECK(company, cdsNumber, quantity);
            if (returnVal != "0")
            {
                return returnVal;
            }
            //IF BUY ORDER
            var totalAmountToSpent = decimal.Parse(quantity) * decimal.Parse(price);
            var tempDbContext = new cdscDbContext();
            var moneyAvail =
                    tempDbContext.CashTrans.FirstOrDefault(x => x.CDS_Number == cdsNumber);

            if (moneyAvail != null)
            {
                var theCashBal =
                    tempDbContext.CashTrans.Where(x => x.CDS_Number == cdsNumber)
                        .Select(x => x.Amount)
                        .Sum();
                if (theCashBal <= 0 || theCashBal < totalAmountToSpent)
                {
                    return "You have insufficient balance in your Cash account";
                }
            }
            else
            {
                return "You have insufficient balance in your Cash account";
            }

            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                //String mycds_ = GetCdsNumberFROMMOBILE(mobile);
                SqlCommand cmdn = new SqlCommand();
                cmdn.Connection = connection;
                cmdn.CommandType = CommandType.StoredProcedure;
                cmdn.CommandText = "AReceiveBidsIPO";
                cmdn.Parameters.AddWithValue("@Bank", "0");
                cmdn.Parameters.AddWithValue("@Branch", "0");
                cmdn.Parameters.AddWithValue("@Accountnumber", "0");
                cmdn.Parameters.AddWithValue("@No_of_Notes_Applied", quantity);
                cmdn.Parameters.AddWithValue("@AmountPaid", tot);
                cmdn.Parameters.AddWithValue("@PaymentRefNo", "Escrow");
                cmdn.Parameters.AddWithValue("@ClientType", "LI");
                cmdn.Parameters.AddWithValue("@BrokerReference", "OMSEC");
                cmdn.Parameters.AddWithValue("@DividendDisposalPreference", "M");
                cmdn.Parameters.AddWithValue("@MNO_", "ANDROID");
                cmdn.Parameters.AddWithValue("@Identification", cdsNumber);
                cmdn.Parameters.AddWithValue("@TelephoneNumber", "");
                cmdn.Parameters.AddWithValue("@CDSC_Number", cdsNumber);
                cmdn.Parameters.AddWithValue("@ReceiptNumber", "Escrow");
                cmdn.Parameters.AddWithValue("@Company", company);
                //cmdn.Parameters.AddWithValue("@Custodian", "0");
                //cmdn.Parameters.AddWithValue("@TransNum", "0");
                //cmdn.Parameters.AddWithValue("@PledgeIndicator", "0");
                //cmdn.Parameters.AddWithValue("@PledgeeBPID", cdsNumber);
                connection.Open();
                if (cmdn.ExecuteNonQuery() > 0)
                {
                    connection.Close();
                    //save to cdsc tempPreorderLive too
                    var orderCashTrans = new CashTrans
                    {
                        Description = "IPO - BUY",
                        TransType = "BUY",
                        TransStatus = "1",
                        Amount = -totalAmountToSpent,
                        CDS_Number = cdsNumber,
                        DateCreated = DateTime.Now
                    };

                    tempDbContext.CashTrans.Add(orderCashTrans);
                    tempDbContext.SaveChanges();
                    return "Order placed successfully";
                }
                else
                {
                    connection.Close();
                    return "Error placing order";
                }
            }

        }

        public string GetCdsNumber(string cdsNumber)
        {
            var getBrk =
                "SELECT TOP 1 [CDS_Number], [Surname] , [Forenames] FROM [CDS_ROUTER].[dbo].[Accounts_Clients] where CDS_Number = '" +
                cdsNumber + "'";
            var order = new Accounts_Clients();
            var ret = "";

            using (var connection = new SqlConnection(connectionString))
            {
                var cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                var rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        //                        order.CDS_Number = rdr["CDS_Number"].ToString();
                        ret += rdr["Surname"].ToString() + " " + rdr["Forenames"].ToString();

                        //                        order.Surname = rdr["Surname"].ToString();
                        //                        order.Forenames = rdr["Forenames"].ToString();
                        //                        listOfOrders.Add(order);
                    }

                    try
                    {
                        //                        var jsonResult = Json(listOfOrders, JsonRequestBehavior.AllowGet);
                        //                        jsonResult.MaxJsonLength = int.MaxValue;
                        return ret;
                    }
                    catch (Exception)
                    {
                        return "1";
                    }
                }
                else
                {
                    return "0";
                }
            }
        }
        public JsonResult getAllDetails(string cdsNumber)
        {
            var getBrk =
                "SELECT TOP 1 * From Accounts_Clients where CDS_Number = '" +
                cdsNumber + "'";
            var order = new Accounts_Clients_MYPROF();
            var listOfBrokers = new List<Accounts_Clients_MYPROF>();
            using (var connection = new SqlConnection(connectionString))
            {
                var cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                var rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        order.BrokerCode = rdr["BrokerCode"].ToString();
                        order.Title = rdr["Title"].ToString();
                        order.Nationality = rdr["Nationality"].ToString();
                        order.Add_1 = rdr["Add_1"].ToString();
                        order.Country = rdr["Country"].ToString();
                        order.Mobile = rdr["Mobile"].ToString();
                        order.Email = rdr["Email"].ToString();
                        order.Custodian = rdr["Custodian"].ToString();
                        order.Cash_Bank = rdr["Cash_Bank"].ToString();
                        order.Cash_Branch = rdr["Cash_Branch"].ToString();
                        order.Cash_AccountNo = rdr["Cash_AccountNo"].ToString();
                        order.idnopp = rdr["idnopp"].ToString();
                        order.mobile_number = rdr["mobile_number"].ToString();
                        order.CDS_Number = rdr["CDS_Number"].ToString();
                        order.Surname = rdr["Surname"].ToString();
                        order.Forenames = rdr["Forenames"].ToString();
                        listOfBrokers.Add(order);
                    }

                    try
                    {
                        var jsonResult = Json(listOfBrokers, JsonRequestBehavior.AllowGet);
                        jsonResult.MaxJsonLength = int.MaxValue;
                        return jsonResult;
                    }
                    catch (Exception)
                    {
                        return Json("Error loading details", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Error loading details", JsonRequestBehavior.AllowGet);
                }
            }
        }



        public JsonResult GetBrokers(string company = null)
        {
            var getBrk =
                "  select Company_Code as Code, upper(Company_name) as Name  FROM [CDS_ROUTER].[dbo].[Client_Companies] where Company_type='BROKER'";


            var listOfBrokers = new List<Broker>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Broker broker = new Broker();
                    broker.Code = rdr["Code"].ToString();
                    broker.Name = rdr["Name"].ToString();
                    listOfBrokers.Add(broker);
                }

                try
                {
                    var jsonResult = Json(listOfBrokers, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception)
                {
                    return Json("Data set too large", JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetCustodian(string company = null)
        {
            var getBrk =
                "select Company_Code as Code, upper(Company_name) as Name  FROM [CDS_ROUTER].[dbo].[Client_Companies] where Company_type='CUSTODIAN'";

            var listOfBrokers = new List<Broker>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    Broker broker = new Broker();
                    broker.Code = rdr["Code"].ToString();
                    broker.Name = rdr["Name"].ToString();
                    listOfBrokers.Add(broker);
                }

                try
                {
                    var jsonResult = Json(listOfBrokers, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception)
                {
                    return Json("Data set too large", JsonRequestBehavior.AllowGet);
                }
            }
        }
        public JsonResult GetCompaniies(string company = null)
        {
            var atsDbContext = new AtsDbContext();
            //Selecting distinct
            var companies = atsDbContext.para_company.Select(c =>
                new
                {
                    CompayCode = c.Company,
                    CompanyName = c.fnam
                });
            return Json(companies, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSecurities(String exchange, string company = null)
        {
            var atsDbContext = new AtsDbContext();
            var statement =
                atsDbContext.para_company.OrderByDescending(x => x.ID).Where(x => x.fnam == company && x.exchange == exchange).Select(c => new
                {
                    Id = c.ID,
                    Instrument = c.Instrument,
                    IsinNo = c.Company,
                    InitialPrice = c.InitialPrice
                });
            return Json(statement, JsonRequestBehavior.AllowGet);
        }

        //ACCOUNT CREATION PARAMETERES
        public JsonResult GetMyCompanies(string exchange, string company = null)
        {
            var atsDbContext = new AtsDbContext();
            //Selecting distinct
            var companies =
                atsDbContext.para_company.Where(x => x.exchange == exchange).GroupBy(x => x.fnam).Select(x => x.FirstOrDefault()).Select(c =>
                new
                {
                    Id = c.ID,
                    compInitial = c.Company,
                    Name = c.fnam,
                    InitialPrice = c.InitialPrice
                });
            return Json(companies, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCompanyFullName(string exchange)
        {
            var atsDbContext = new AtsDbContext();
            //Selecting distinct
            var companies =
                atsDbContext.para_company.Where(x => x.exchange == exchange).GroupBy(x => x.fnam).Select(x => x.FirstOrDefault()).Select(c =>
                new
                {
                    Id = c.ID,
                    company = c.Company,
                    Name = c.fnam,
                    InitialPrice = c.InitialPrice
                });
            return Json(companies, JsonRequestBehavior.AllowGet);
        }

        //ACCOUNT CREATION PARAMETERES
        public string Widthdraw(string cdsnumber, string ammount)
        {
            var tempDbContext = new cdscDbContext();
            try
            {

                var moneyAvail =
                        tempDbContext.CashTrans.FirstOrDefault(x => x.CDS_Number == cdsnumber);

                if (moneyAvail != null)
                {
                    var theCashBal =
                        tempDbContext.CashTrans.Where(x => x.CDS_Number == cdsnumber)
                            .Select(x => x.Amount)
                            .Sum();
                    if (theCashBal <= 0 || theCashBal < (Decimal.Parse(ammount)))
                    {
                        return "You have insufficient balance in your Cash account";
                    }
                }
                else
                {
                    return "You have insufficient balance in your Cash account";
                }

                //Selecting distinct
                var orderCashTrans = new CashTrans
                {
                    Description = "Withdrawal",
                    TransType = "Withdraw",
                    TransStatus = "1",
                    Amount = -(Decimal.Parse(ammount)),
                    CDS_Number = cdsnumber,
                    DateCreated = DateTime.Now
                };
                tempDbContext.CashTrans.Add(orderCashTrans);
                tempDbContext.SaveChanges();
                return "Withdraw Successfull";
            }
            catch (Exception ex)
            {
                return "Failed to make withdrawal" + ex;
            }
            //return Json(companies, JsonRequestBehavior.AllowGet);
        }

        //ACCOUNT CREATION PARAMETERES
        public JsonResult GetCompaniesListForGraph(string exchange)
        {
            var atsDbContext = new AtsDbContext();
            //Selecting distinct
            var companies =
                atsDbContext.para_company.Where(x => x.exchange == exchange).GroupBy(x => x.fnam).Select(x => x.FirstOrDefault()).Select(c =>
                new
                {
                    Id = c.ID,
                    Name = c.Company,
                    InitialPrice = c.InitialPrice,
                    Instrument = c.Instrument,

                });
            return Json(companies, JsonRequestBehavior.AllowGet);
        }
        //ACCOUNT CREATION PARAMETERES trading_platform

        public JsonResult GetCompaniesListForGraphAll()
        {
            var atsDbContext = new AtsDbContext();
            //Selecting distinct
            var companies =
                atsDbContext.para_company.GroupBy(x => x.fnam).Select(x => x.FirstOrDefault()).Select(c =>
                new
                {
                    Id = c.ID,
                    Name = c.Company,
                    InitialPrice = c.InitialPrice,
                    Instrument = c.Instrument,

                });
            return Json(companies, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCompaniesMaPriceList(string cmpny)
        {
            var atsDbContext = new AtsDbContext();
            //Selecting distinct
            var companies =
                atsDbContext.para_company.Where(x => x.Company == cmpny).Select(c =>
                new
                {
                    InitialPrice = c.InitialPrice
                });
            return Json(companies, JsonRequestBehavior.AllowGet);
        }


        public string GetTradingPlaform(string company)
        {
            var getBrk =
                "SELECT exchange FROM para_company where Company  = '" +
                company.Trim() + "'";

            var ret = "";

            using (var connection = new SqlConnection(connectionStringATS))
            {
                var cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                var rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        ret += rdr["exchange"];
                    }

                    try
                    {
                        return ret;
                    }
                    catch (Exception)
                    {
                        return "FINSEC";
                    }
                }
                return "FINSEC";
            }
        }



        //ACCOUNT CREATION PARAMETERES
        public JsonResult GetCompaniesPriceList(string company)
        {
            var getBrk = @"  select DISTINCT CONVERT(date , b.price_date) as date_ ,(select TOP 1 a.[price_close] from Prices a WHERE CONVERT(date , a.price_date)=CONVERT(date , b.price_date) and a.company_name=b.company_name ORDER BY a.price_date DESC) AS PriceClose
  from prices b
  WHERE b.company_name = '" + company + @"'";

            var listOfBrokers = new List<MYPriceGRAPH>();
            using (SqlConnection connection = new SqlConnection(connectionStringATS))
            {
                SqlCommand cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MYPriceGRAPH broker = new MYPriceGRAPH();
                    broker.Price = rdr["PriceClose"].ToString();
                    broker.Date = DateTime.Parse(rdr["date_"].ToString()).ToString("dd/MM/yyyy");
                    listOfBrokers.Add(broker);
                }

                try
                {
                    var jsonResult = Json(listOfBrokers, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception)
                {
                    return Json("Data set too large", JsonRequestBehavior.AllowGet);
                }
            }

        }

        public JsonResult GetCompaniesPriceFromList(string company)
        {
            var getBrk = @" select TOP 1 DISTINCT CONVERT(date , b.price_date) as date_ ,(select TOP 1 a.[price_close] from Prices a WHERE CONVERT(date , a.price_date)=CONVERT(date , b.price_date) and a.company_name=b.company_name ORDER BY a.price_date ASC) AS PriceClose
  from prices b
  WHERE b.company_name = '" + company + @"'";

            var listOfBrokers = new List<MYPriceGRAPH>();
            using (SqlConnection connection = new SqlConnection(connectionStringATS))
            {
                SqlCommand cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    MYPriceGRAPH broker = new MYPriceGRAPH();
                    broker.Price = rdr["PriceClose"].ToString();
                    broker.Date = DateTime.Parse(rdr["date_"].ToString()).ToString("dd/MM/yyyy");
                    listOfBrokers.Add(broker);
                }

                try
                {
                    var jsonResult = Json(listOfBrokers, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception)
                {
                    return Json("Data set too large", JsonRequestBehavior.AllowGet);
                }
            }

        }


        [HttpGet]
        public async System.Threading.Tasks.Task<ActionResult> PaynowPayments(string cdsNumber, string price, string quantity, string email)
        {
            var cdstDbContext = new cdscDbContext();


            if (cdsNumber != null || price != null || quantity != null || email != null)
            {
                var total = Math.Round(decimal.Parse(price) * decimal.Parse(quantity), 2);
                var buyOrderPayment = new BuyOrderPayments
                {
                    CdsNumber = cdsNumber,
                    Company = "None",
                    Price = price,
                    Security = "Equity",
                    Total = total.ToString(CultureInfo.InvariantCulture),
                    Quantity = quantity,
                    PaymentStatus = "PENDING",
                    PostedStatus = "PENDING",
                    Date = DateTime.Now,
                    Broker = "None",
                    EmailAddress = email
                };

                cdstDbContext.BuyOrderPayments.Add(buyOrderPayment);
                cdstDbContext.SaveChanges();

                var cdstDbContextc = new cdscDbContext();
                var order =
                    cdstDbContextc.BuyOrderPayments.OrderByDescending(x => x.Id)
                        .FirstOrDefault(
                            x => x.CdsNumber == cdsNumber && x.PaymentStatus == "PENDING" && x.PostedStatus == "PENDING");

                if (order != null)
                {
                    var obj = Guid.NewGuid();
                    //TEST
                    //                var theId = 3842;
                    //                var guidGuid = "6158d615-d21c-41ab-851d-bb40f81b8b2d";

                    //LIVE


                    var theId = 3736;
                    var guidGuid = "9ab1c705-ec9b-4fdb-901a-e20d9523a5ed";
                    var hashData = new Dictionary<string, string>
                {
                    {"id", theId.ToString()},
                    {"reference", order.Id.ToString()},
                    {"amount", total.ToString(CultureInfo.InvariantCulture)},
                    {"returnurl", "http://197.155.235.78:8446/CTRADEAPI/subscriber/PaynowPaymentsReturnUrl"},
                    {"resulturl", "http://197.155.235.78:8446/CTRADEAPI/subscriber/PaynowPaymentsResultUrl"},
                    {"status", "Message"}
                };
                    var hash = GenerateTwoWayHash(hashData, guidGuid);
                    //            http://192.168.47.1/SSX/Ssx/PaynowPaymentsReturnUrl#

                    //192.168.47.1
                    //192.168.3.245
                    //197.155.235.78:8446


                    var postData = new Dictionary<string, string>
                {
                    {"id", theId.ToString()},
                    {"reference", order.Id.ToString()},
                    {"amount", total.ToString(CultureInfo.InvariantCulture)},
                    {"returnurl", "http://197.155.235.78:8446/CTRADEAPI/subscriber/PaynowPaymentsReturnUrl"},
                    {"resulturl", "http://197.155.235.78:8446/CTRADEAPI/subscriber/PaynowPaymentsResultUrl"},
                    {"status", "Message"},
                    {"hash", hash}
                };

                    var content = new FormUrlEncodedContent(postData);

                    var response =
                        await client.PostAsync("https://www.paynow.co.zw/interface/initiatetransaction", content);

                    var responseString = await response.Content.ReadAsStringAsync();
                    string decodedUrl = HttpUtility.UrlDecode(responseString);

                    var urlToGo = new List<string>();
                    if (decodedUrl != null)
                    {
                        urlToGo.AddRange(decodedUrl.Split('&'));
                        return this.Redirect(urlToGo[1].Split('=')[1].Trim());
                    }
                    else
                    {
                        return View();
                    }
                }

                //ViewData["total"] = total;

                //ViewData["cdsNumber"] = cdsNumber;
                //ViewData["security"] = "Equity";
                //ViewData["quantity"] = quantity;
                //ViewData["company"] = "None";
                //ViewData["price"] = price;
            }
            else
            {
                return View();
            }
            return View();
        }


        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> PaynowPayments(FormCollection formCollection)
        //        public string PaynowPayments(string cdsNumber, string company, string price, decimal quantity, string security, string securityz)
        {
            var cdsNumber = formCollection["cdsNumber"];
            var total = formCollection["total"];
            var security = formCollection["security"];
            var quantity = formCollection["quantity"];
            var company = formCollection["company"];
            var price = formCollection["price"];

            var cdstDbContext = new cdscDbContext();
            var order =
                cdstDbContext.BuyOrderPayments.OrderByDescending(x => x.Id)
                    .FirstOrDefault(
                        x => x.CdsNumber == cdsNumber && x.PaymentStatus == "PENDING" && x.PostedStatus == "PENDING");

            if (order != null)
            {
                var obj = Guid.NewGuid();
                //TEST
                //                var theId = 3842;
                //                var guidGuid = "6158d615-d21c-41ab-851d-bb40f81b8b2d";

                //LIVE


                var theId = 3736;
                var guidGuid = "9ab1c705-ec9b-4fdb-901a-e20d9523a5ed";
                var hashData = new Dictionary<string, string>
                {
                    {"id", theId.ToString()},
                    {"reference", order.Id.ToString()},
                    {"amount", total.ToString(CultureInfo.InvariantCulture)},
                    {"returnurl", "http://197.155.235.78:8446/SSX/Ssx/PaynowPaymentsReturnUrl"},
                    {"resulturl", "http://197.155.235.78:8446/SSX/Ssx/PaynowPaymentsResultUrl"},
                    {"status", "Message"}
                };
                var hash = GenerateTwoWayHash(hashData, guidGuid);
                //            http://192.168.47.1/SSX/Ssx/PaynowPaymentsReturnUrl#

                //192.168.47.1
                //192.168.3.245
                //197.155.235.78:8446


                var postData = new Dictionary<string, string>
                {
                    {"id", theId.ToString()},
                    {"reference", order.Id.ToString()},
                    {"amount", total.ToString(CultureInfo.InvariantCulture)},
                    {"returnurl", "http://197.155.235.78:8446/SSX/Ssx/PaynowPaymentsReturnUrl"},
                    {"resulturl", "http://197.155.235.78:8446/SSX/Ssx/PaynowPaymentsResultUrl"},
                    {"status", "Message"},
                    {"hash", hash}
                };

                var content = new FormUrlEncodedContent(postData);

                var response =
                    await client.PostAsync("https://www.paynow.co.zw/interface/initiatetransaction", content);

                var responseString = await response.Content.ReadAsStringAsync();
                string decodedUrl = HttpUtility.UrlDecode(responseString);

                var urlToGo = new List<string>();
                if (decodedUrl != null)
                {
                    urlToGo.AddRange(decodedUrl.Split('&'));
                    return this.Redirect(urlToGo[1].Split('=')[1].Trim());
                }
                else
                {
                    return View();
                }
            }
            return View();
        }


        private static string GenerateTwoWayHash(Dictionary<string, string> items, string guid)
        {
            string concat = string.Join("",
                items.Select(c => (c.Value != null && c.Key.ToLower() != "hash" ? c.Value.Trim() : "")).ToArray());
            SHA512 check = SHA512.Create();
            byte[] resultArr = check.ComputeHash(Encoding.UTF8.GetBytes(concat + guid));
            return ByteArrayToString(resultArr);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString().ToUpper();
        }

        //        [HttpPost]
        public string PaynowPaymentsReturnUrl()
        {
            return "Thank you for paying with paynow";
        }


        [HttpPost]
        public void PaynowPaymentsResultUrl(FormCollection formCollection)
        {
            var csdhDbContext = new cdscDbContext();

            try
            {
                var reference = formCollection["reference"];
                var amount = formCollection["amount"];
                var status = formCollection["status"];
                var pollurl = formCollection["pollurl"];
                var paynowreference = formCollection["paynowreference"];


                //                var buyOrderPayments = new BuyOrderPayment
                //                {
                //                    PaynowRef = paynowreference,
                //                    PollUrl = pollurl,
                //                    PaymentStatus = status,
                //                    Total = amount
                //                };
                //
                //                ssxDbContext.BuyOrderPayments.Add(buyOrderPayments);
                //                ssxDbContext.SaveChanges();

                var intRef = int.Parse(reference);

                var deal = csdhDbContext.BuyOrderPayments.OrderByDescending(x => x.Id)
                    .FirstOrDefault(x => x.Id == intRef);

                if (deal != null)
                {
                    deal.PaynowRef = paynowreference;
                    deal.PollUrl = pollurl;
                    deal.PaymentStatus = status;

                    deal.Total = amount;
                    csdhDbContext.SaveChanges();


                    if (status.Equals("Awaiting Delivery") || status.Equals("Paid") || status.Equals("Delivered"))
                    {
                        var orderCashTrans = new CashTrans
                        {
                            Description = "PAYNOW DEPOSIT",
                            TransType = "DEPOSIT",
                            TransStatus = "1",
                            Amount = decimal.Parse(deal.Total),
                            CDS_Number = deal.CdsNumber,
                            DateCreated = DateTime.Now
                        };
                        csdhDbContext.CashTrans.Add(orderCashTrans);
                        csdhDbContext.SaveChanges();
                        deal.PostedStatus = status;
                        csdhDbContext.SaveChanges();

                    }
                    else
                    {
                        //SendMail2(deal.EmailAddress,
                        //    "Your order was not successfully posted. Your got the following status: " + status +
                        //    " Paynow", "Order Posting");
                    }
                }
                else
                {
                    var buyOrderPayment = new BuyOrderPayments
                    {
                        PaymentStatus = status,
                        PaynowRef = "Not Null" + intRef
                    };

                    csdhDbContext.BuyOrderPayments.Add(buyOrderPayment);
                    csdhDbContext.SaveChanges();
                }
            }
            catch (Exception e)
            {
                var buyOrderPayment = new BuyOrderPayments
                {
                    PaymentStatus = e.Message
                };

                csdhDbContext.BuyOrderPayments.Add(buyOrderPayment);
                csdhDbContext.SaveChanges();
            }
        }



        private string LIMITS_CHECK(string isin, string cds_acc, string quantity)
        {
            string return_str = "";
            var connectionString = ConfigurationManager.ConnectionStrings["CdDataContext"].ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("declare @cdsnumber nvarchar(50)='" + cds_acc + "' declare @issue nvarchar(50)='" + isin + "' select Bidratio, IPOSTATUS, IndividualLimit, GlobalLimit, globLowerlimit, DailyLimit,Transaction_Limit,FirstLimit,isnull((select cds_number from Accounts_Clients where CDS_Number=@cdsnumber),'NotExisting') as [CDSExist], (SELECT isnull(SUM(isnull(No_of_Notes_Applied,0)),0) as BidToDate FROM [Bond_Payment_Audit] where Company =@issue) as [Bidstodate],(SELECT isnull(SUM(isnull(No_of_Notes_Applied,0)),0) as BidToDate FROM [Bond_Payment_Audit] where Company =@issue and CDSC_Number=@cdsnumber) as [InvestorTotalBidsGlobal],(SELECT isnull(SUM(isnull(No_of_Notes_Applied,0)),0) as BidToDate FROM [Bond_Payment_Audit] where Company =@issue and CDSC_Number=@cdsnumber and convert(date,date_created)=convert(date,getdate())) as InvestorTotalToday, isnull((select Mobile from Accounts_Clients where CDS_Number=@cdsnumber),'NotExisting') as [Telephone], isnull((select IDNoPP from Accounts_Clients where CDS_Number=@cdsnumber),'NotExisting') as [Idnumber]  from para_holding WITH (NOLOCK) where NewIssuerCode=@issue", con))
                {
                    DataSet dsi = new DataSet();
                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(dsi, "para_holding");
                    if (dsi.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = dsi.Tables[0].Rows[0];
                        if (Convert.ToInt32(dr["IPOSTATUS"]) == 1)
                        {
                            return_str = "Primary Issuance Has Been Closed";
                        }
                        else if (dr["CDSExist"].ToString() == "NotExisting")
                        {
                            return_str = "CDS Account Number does Not Exist";
                        }
                        else if (Convert.ToInt64(dr["Bidstodate"]) >= Convert.ToInt64(dr["GlobalLimit"]))
                        {
                            return_str = "Global Limit Already Reached";
                        }
                        else if (Convert.ToInt64(dr["Bidstodate"]) + Convert.ToInt64(quantity) > Convert.ToInt64(dr["GlobalLimit"]))
                        {
                            return_str = "Quantity will exceed Global Limit";
                        }
                        else if (Convert.ToInt64(dr["InvestorTotalBidsGlobal"]) >= Convert.ToInt64(dr["IndividualLimit"]))
                        {
                            return_str = "Individual Limit Already Reached";
                        }
                        else if (Convert.ToInt64(dr["InvestorTotalBidsGlobal"]) + Convert.ToInt64(quantity) > Convert.ToInt64(dr["IndividualLimit"]))
                        {
                            return_str = "Individual Limit will be exceeded";

                        }
                        else if (Convert.ToInt64(dr["InvestorTotalToday"]) >= Convert.ToInt64(dr["DailyLimit"]))
                        {
                            return_str = "Daily Limit Already Reached";

                        }
                        else if (Convert.ToInt64(dr["InvestorTotalToday"]) + Convert.ToInt64(quantity) > Convert.ToInt64(dr["DailyLimit"]))
                        {
                            return_str = "Daily Limit will be exceeded";

                        }
                        //else if (Convert.ToInt64(dr["InvestorTotalBidsGlobal"]) == 0 & Convert.ToInt64(quantity) < Convert.ToInt64(dr["FirstLimit"]))
                        //{
                        //    return_str = "First Bid. The Amount Entered is Less than the First Bid limit which is " + dr["FirstLimit"].ToString();

                        //}
                        else if (Convert.ToInt64(quantity) < Convert.ToInt64(dr["globLowerlimit"]))
                        {
                            return_str = "The Amount Entered is Less than the Lower Bid limit which is " + dr["globLowerlimit"].ToString();

                        }
                        else
                        {
                            return_str = "0";
                        }
                    }
                    else
                    {
                        return_str = "Limits Not set";

                    }
                }
            }
            return return_str;
        }

        public static string Force4DecimalPlaces(string input)
        {
            decimal parsed = decimal.Parse(input, CultureInfo.InvariantCulture);
            string intermediate = parsed.ToString("0.0000", CultureInfo.InvariantCulture);
            return decimal.Parse(intermediate, CultureInfo.InvariantCulture).ToString();
        }

        public static string Force2DecimalPlaces(string input)
        {
            decimal parsed = decimal.Parse(input, CultureInfo.InvariantCulture);
            string intermediate = parsed.ToString("0.00", CultureInfo.InvariantCulture);
            return decimal.Parse(intermediate, CultureInfo.InvariantCulture).ToString();
        }

        public string GetCdsNumberFROMIDNUM(string idnumb)
        {
            var getBrk =
                "SELECT TOP 1 [CDS_Number] FROM [CDS_ROUTER].[dbo].[Accounts_Clients] where IDNoPP = '" +
                idnumb + "'";
            var order = new Accounts_Clients();
            var ret = "";
            using (var connection = new SqlConnection(connectionString))
            {
                var cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                var rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        ret += rdr["CDS_Number"].ToString();
                    }

                    try
                    {
                        return ret;
                    }
                    catch (Exception)
                    {
                        return "1";
                    }
                }
                else
                {
                    return "0";
                }
            }
        }
        public string getCurrentBalance(string company, string cdsnumber)
        {
            var getBrk =
                "select isnull(Net,0) as NetShares from cdsc.dbo.MyportfolioAll d where d.CDS_Number='" +
                cdsnumber.Trim() + "' AND D.COMPANY = '" + company.Trim() + "'";

            var ret = "";

            using (var connection = new SqlConnection(connectionStringATS))
            {
                var cmd = new SqlCommand(getBrk, connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;
                connection.Open();
                var rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        ret = rdr["NetShares"].ToString();
                    }

                    try
                    {
                        return ret;
                    }
                    catch (Exception)
                    {
                        return "0";
                    }
                }
                return "0";
            }
        }


    }
}