namespace Mpoko.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UssdRegsAll
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
      
        public string SessionId { get; set; }
   
        public string Msisdn { get; set; }

        public string NatId { get; set; }

        public string CdsNumber { get; set; }

        public string Pin { get; set; }

        public string Broker { get; set; }

        public Boolean Approved { get; set; }

        public Boolean Sent { get; set; }

        public Boolean Accepted { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date { get; set; }

        public string Error { get; set; }

    }

    public partial class UssdRegs
    {
  
        public string Msisdn { get; set; }

        public string NatId { get; set; }

        public string CdsNumber { get; set; }

        public string OtherNames { get; set; }

        public string Surname { get; set; }

    }

}
