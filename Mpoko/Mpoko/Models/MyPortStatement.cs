﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class MyPortStatement
    {
        public string id { get; set; }
        public string counter { get; set; }
        public string date { get; set; }
        public string pricepershare { get; set; }
        public string volume { get; set; }
        public string purchasevalue { get; set; }
        public string currentmarketprice { get; set; }
        public string returns { get; set; }
    }
}