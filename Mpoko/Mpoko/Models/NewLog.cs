﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class NewLog
    {

        public string Id { get; set; }
        public string BrokerCode { get; set; }
        public string CdsNumber { get; set; }
    }
}