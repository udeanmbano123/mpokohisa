﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class bidPrices
    {
        public string id { get; set; }
        public string price { get; set; }
        public string volume { get; set; }
    }

    public class bidPrices_correct
    {
        public string id { get; set; }
        public string price { get; set; }
        public string volume { get; set; }
    }
}