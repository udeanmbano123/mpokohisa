namespace Mpoko.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class cdscDbContext : DbContext
    {
        public cdscDbContext()
            : base("name=cdscDbContext")
        {
        }

        public DbSet<Subscriber> Subscribers { get; set; }
        public DbSet<CashTrans> CashTrans { get; set; }
        public DbSet<PreOrderLives> PreOrderLives { get; set; }
        public DbSet<PreOrderLivesIPOes> PreOrderLivesIPOes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<SubscriberProduct> SubscriberProducts { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<BuyOrderPayments> BuyOrderPayments { get; set; }
        public DbSet<Test> Test { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<cdscDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<Mpoko.Models.MarketNews> MarketNews { get; set; }
    }
}
