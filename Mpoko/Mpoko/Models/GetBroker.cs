﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class GetBroker
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}