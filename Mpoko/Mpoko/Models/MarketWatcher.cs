﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mpoko.Models
{
    public class MarketWatcher
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string SharesInIssue { get; set; }
        public decimal ClosingPrice { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal Change { get; set; }

        public string ChangeString { get; set; }

        public float ChangeFloat { get; set; }
        public string CompanyName { get; set; }
        public string Exchange { get; set; }
    }

    public class Subscribers
    {
        public string Username { get; set; }
        public string CdsNumber { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Dob { get; set; }
        public string Password { get; set; }
        public Boolean SmsOpt { get; set; }
    }

    public class ESCROWCLIENTS
    {
        public string ACCOUNT { get; set; }
        public string ADDRESS { get; set; }
        public string BANK { get; set; }
        public string BRANCH { get; set; }
        public string CLIENT_PREFIX { get; set; }
        public string CLIENT_SUFFIX { get; set; }
        public string DATE_CHANGED { get; set; }
        public string DATE_OF_REGISTRATION { get; set; }

        public string CLIENT_TYPE { get; set; }
        public string e_MAIL { get; set; }
        public string MEMBER_CODE { get; set; }
        public string MEMBER_TYPE { get; set; }
        public string OTHER_NAMES { get; set; }
        public string SURNAME { get; set; }
        public string TELEPHONE { get; set; }
        public string USERNAME { get; set; }
        public string DOB { get; set; }
    }

    public class APPLIED_LOANS
    {
        public int id { get; set; }
        public string counter { get; set; }
        public string shares_loaned { get; set; }
        public string share_price { get; set; }
        public string loan_amount { get; set; }
        public string repayment_period { get; set; }
        public string interest { get; set; }
        public string created_date { get; set; }
        public string first_repay_date { get; set; }
        public string status { get; set; }
    }

    public class DATASET_HOLDINGS
    {
        public string MEMBER_CODE { get; set; }
        public string SHORT_NAME { get; set; }
        public string ISSUER_CODE { get; set; }
        public string qnty_held { get; set; }
        public string DATE_LAST_TRANSACTION { get; set; }
    }

    public class MarketWatcherZSE
    {
        public int id { get; set; }
        public string Ticker { get; set; }
        public string ISIN { get; set; }
        public string Best_Ask { get; set; }
        public string Best_bid { get; set; }
        public string Current_price { get; set; }
        public string CompanyFullName { get; set; }

    }


}